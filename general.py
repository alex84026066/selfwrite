import numpy as np
import pandas as pd
import sys
from scipy.spatial.distance import cdist
import pickle
from sklearn.metrics import mean_squared_error
import twd97
import os

def checkdf2array(*args):
    '''Change input to numpy array if input is dataframe.
    
    Syntax
    ------
        [array1,array2,array3] = checkdf2array(df1,df2,df3)
    
    '''
    oplist = []
    for i,X in enumerate(args):
        if isinstance(X, pd.DataFrame) or isinstance(X, pd.Series):
             X = X.values
        oplist.append(X)
    if i==0:  
        oplist = oplist[0]
    return oplist

def df2npar(Xlist):
    def _df2npar(X):
        if isinstance(X,pd.DataFrame):
            ocX = X.values
        elif isinstance(X,pd.Series):
            ocX = X.values
        else:
            ocX = X.copy()
        return ocX
    '''
    check if input is DataFrame.
    If Ture. Turn to numpy array.
    '''
    if not isinstance(Xlist,list):
        Xlist = [Xlist]
    oclist = []
    for X in Xlist:
        oclist.append(_df2npar(X))
    return oclist
    
def differ(TimeSeries):
	return TimeSeries[1:]-TimeSeries[:-1]
	
def int2_nlen_str(Num,nlen):
	'''
	change int to n length integer string
	'''
	intlen = len(str(int(Num)))
	zero_num = nlen - intlen
	output = '0'*zero_num + str(Num)
	return output

def chwarn_filter(Warnings_Filter = 'error'):
	'''
	change the waring filter when encounter warning
	ref:
	https://docs.python.org/3/library/warnings.html
	'''
	if not sys.warnoptions:
		import warnings
		warnings.simplefilter(Warnings_Filter)

def find_nearest_point(x,space,order = 0):
	'''
	Input
		x:     
			[1 x 2] 1D or 2D numpy array.
		space: 
			[n x 2] Dataframe.
			index must be the station name
		order:
			int.
			distance order you want to get.
			If order == 0, means the nearest point.
			If order == 1, means the second near point.
	'''
	if type(x) == type(pd.DataFrame()):
		x = x.values
	dis_array = cdist(x.reshape(1,2),space.values)
	min_dis = np.sort(dis_array)[0][order]
	nearest_staname = space.loc[(dis_array==min_dis)[0],:].index
	return nearest_staname[0],min_dis

def normalize(timeseries):
	return (timeseries - timeseries.min())/(timeseries.max()-timeseries.min())

def nearestneighbor(s1,s2,uni_oc = True):
	'''
	Find the nearest point's index
	Input:
		s1.  [n1 x d]
			DataFrame.
			One set of coordinate that you want find nearest neighbor.
		s2.  [n2 x d]
			Dataframe.      
			The coordinate pool you going to find it.
		uni_oc. boolen
			If True,take the first station when find mutiple nearest station.
	'''
	if uni_oc:
		s2 = s2.drop_duplicates()
	dis_array = cdist(s1.values,s2.values)
	min_dis = dis_array.min(axis = 1)
	nearest_point_index = []
	for i in np.arange(len(s1)):
		nearest_point_ = s2.index[dis_array[i,:]==min_dis[i]]
		if not uni_oc:
				nearest_point_index.append(nearest_point_.tolist())
		else:
			nearest_point_index.append(nearest_point_[0])
	found_point_index = s1.index

	return found_point_index, nearest_point_index

def RW_pkl(name,mode,data = None):
    if mode[0]=='w':
        with open(name,'wb') as f1:
            pickle.dump(data,f1)
            f1.close()
    if mode[0]=='r':
        with open(name,'rb') as f1:
            Output_Data = pickle.load(f1)
            f1.close()
        return Output_Data

def RMSE(y_ture,y_pdict,ignore_nan = True):
    if not isinstance(y_ture, np.ndarray):
        y_ture = np.array(y_ture)
    if not isinstance(y_ture, np.ndarray):
        y_pdict = np.array(y_pdict)
    y_ture,y_pdict = y_ture.ravel(),y_pdict.ravel()
    return mean_squared_error(y_ture,y_pdict)

def makedir(str1):
	dirpath = str1
	if not os.path.isdir(dirpath):
		os.mkdir(dirpath)
		
def rmdir(str1):
	import shutil
	shutil.rmtree(str1, ignore_errors=True)

def twd67_to_twd97(matrix,inverse = False):
	#twd67&twd97 is TM2
	A,B= [0.00001549,0.000006521]
	if type(matrix) == type(pd.DataFrame([])):
		Mindex = matrix.index
		Mcol = matrix.columns
		converted_matrix = matrix.values.reshape(-1,2).copy()
	else:
		converted_matrix = matrix.reshape(-1,2).copy()
	length = converted_matrix.shape[0]	
	for i in np.arange(length):
		X,Y = [converted_matrix[i,0],converted_matrix[i,1]]
		if inverse==False:
			X97,Y97 = [X + 807.8 + A * X + B * Y,Y - 248.6 + A * Y + B * X]
			converted_matrix[i,0],converted_matrix[i,1] = [X97,Y97]
		else:
			X67,Y67 = [X - 807.8 - A * X - B * Y,Y + 248.6 - A * Y - B * X]
			converted_matrix[i,0],converted_matrix[i,1] = [X67,Y67]
	if type(matrix) == type(pd.DataFrame([])):
		converted_matrix = pd.DataFrame(converted_matrix,index = Mindex,columns= Mcol)
	return converted_matrix

def wgs84_to_twd97(matrix,inverse = False):
	#twd97 is TM2
	if type(matrix) == type(pd.DataFrame([])):
		Mindex = matrix.index
		Mcol = matrix.columns
		converted_matrix = matrix.values.reshape(-1,2).copy()
	else:
		converted_matrix = matrix.reshape(-1,2).copy()
	for i in np.arange(len(matrix)):
		if inverse == False:
			converted_matrix[i,0],converted_matrix[i,1] = twd97.fromwgs84(converted_matrix[i,1],converted_matrix[i,0])
		if inverse == True:
			converted_matrix[i,1],converted_matrix[i,0] = twd97.towgs84(converted_matrix[i,0],converted_matrix[i,1])	
	if type(matrix) == type(pd.DataFrame([])):
		converted_matrix = pd.DataFrame(converted_matrix,index = Mindex,columns= Mcol)
	return converted_matrix

def twd67_to_wgs84(matrix,inverse = False):
	#twd67 is TM2
	if inverse == False:
		Twd97 = twd67_to_twd97(matrix,inverse = False)
		converted_matrix = wgs84_to_twd97(Twd97,inverse=True)
	else:
		Twd97 = wgs84_to_twd97(matrix,inverse=False)
		converted_matrix = twd67_to_twd97(Twd97,inverse = True)
	return converted_matrix

def SetOverlay(ar1,ar2,method):
    '''
    Deleting ar1's row which also in ar2.
    method could be diff or intersect.
    '''
    if isinstance(ar1,list):
        ar1 = np.array(ar1)
    if isinstance(ar2,list):
        ar2 = np.array(ar2)
    ls1 = ar1.tolist()
    ls2 = ar2.tolist()
    if method=='differ':
        for i in ls2:
            if i in ls1:
                ls1.remove(i)
        op = np.array(ls1)
    elif method =='intersect':
        if len(ls1)>len(ls2):
            smlist,lglist = ls2,ls1
        else:
            smlist,lglist = ls1,ls2
        op = []
        for i in smlist:
            if i in lglist:
                op.append(i)
        op = np.array(op)
    return op


