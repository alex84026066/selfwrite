import gdal, ogr, os, osr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from selfwrite.EOF_analyse import *
from stamps.stest.idw import idw_kdtree
import pandas.tseries.offsets as pto 
from stamps.bme.BMEprobaEstimations import BMEPosteriorMoments
from scipy.interpolate import griddata
from scipy.stats import hmean

fontpath='C:\\Windows\\Fonts\\msjh.ttc'
fontp = fm.FontProperties(family=u'Source Han Sans TW',fname=fontpath)
# input raster left down point
def ResampleRaster(Gridxy,Rasterpath):
    '''
    This function can distract values from raster by given coordinates.
    Input
        Gridxy:
        [s x 2]. numpy array or DataFrame.
        s is point number.

        Rasterpath:
        str. Path of raster.

    Output.
        values:
        [1 x s]. Numpy array.
        Raster values in given coordinates.

    '''
    if isinstance(Gridxy,pd.DataFrame):
        Gridxy = Gridxy.values
    ds = gdal.Open(Rasterpath)
    gt = ds.GetGeoTransform()
    array = ds.ReadAsArray(buf_type=gdal.GDT_Float32)
    ncol,nrow = ds.RasterXSize,ds.RasterYSize
    xseris = np.cumsum([gt[0]]+[gt[1]]*(ncol-1))
    yseris = np.cumsum([gt[3]]+[gt[5]]*(nrow-1))
    valist = []
    for nf in np.arange(len(Gridxy)):
        x_,y_ = Gridxy[nf,:2]
        col,row = np.sum(x_>xseris),np.sum(yseris>y_)
        valist.append(array[row,col])
    values = np.array(valist)
    return values

def Png2Tiff(Pngfile,Tifpath,CRS = 3826):
    '''
    ref :http://drr.ikcest.org/leaf/k8024
    '''
    dataset = gdal.Open(Pngfile)
    width = dataset.RasterXSize
    height = dataset.RasterYSize
    datas = dataset.ReadAsArray(0,0,width,height)
    driver = gdal.GetDriverByName("GTiff")
    tods = driver.Create(Tifpath,width,
        height,3,options=["INTERLEAVE=PIXEL"])
    tods.WriteRaster(0,0,width,height,datas.tostring(),width,height,band_list=[1,2,3])
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(CRS)
    tods.SetProjection(outRasterSRS.ExportToWkt())
    tods.FlushCache()
    outRasterSRS = None
    tods = None
    dataset = None

def array2raster(newRasterfn,rasterOrigin,pixelWidth,pixelHeight,array,CRS):
    cols = array.shape[1]
    rows = array.shape[0]
    originX = rasterOrigin[0]#minx
    originY = rasterOrigin[1]#maxy
    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Float32)
    outRaster.SetGeoTransform((originX, pixelWidth, 0, originY, 0, pixelHeight))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    #outband.SetNoDataValue(-9999)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(CRS)
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()
    outRaster = None
    outband = None
    print('done')


def Calculate_mfK(bom,Kcubic):
    mflayershape = bom.shape
    nlay = mflayershape[0]
    nrow = mflayershape[1]
    ncol = mflayershape[2]
    Kx_mflayer = np.zeros(bom.shape)
    Kz_mflayer = np.zeros(bom.shape)
    for i in np.arange(nrow):
        for j in np.arange(ncol):
            for k in np.arange(nlay):
                if k==0:
                    up_boundz = 0
                down_boundz = np.int(np.round(abs(bom[k,i,j]),decimals=0))
                calKarray = Kcubic[up_boundz:down_boundz,i,j]
                Kx_mflayer[k,i,j] = np.mean(calKarray)
                Kz_mflayer[k,i,j] = hmean(calKarray)
                up_boundz = down_boundz
    return Kx_mflayer,Kz_mflayer

def ScatterplotMFarray(matrix,c  ='blue',label = None):
    rn,cn = matrix.shape
    xx,yy = np.meshgrid(np.arange(cn),np.arange(rn))
    plt.scatter(xx.ravel(),yy[::-1].ravel(),matrix.ravel(),c = c,
        label = label)
    
def TiffOpen(TiffPath,cell_valid_mood=False):
    ds = gdal.Open(TiffPath)
    gt = ds.GetGeoTransform()
    array = ds.ReadAsArray(buf_type=gdal.GDT_Float32)
    ds = None
    if cell_valid_mood==True:
        array = array!=-9999
    return gt,array

def mf_horizontal_Kplot(mf_leftupper_corner,mfsize,Kx=None,Kz=None,basemap=None,folder='Noname'):
    '''
    project K in each layer in a plant,than plot it.
    Input
        Kx,Kz:
        numpy array.

        mf_leftupper_corner:
        tuple
        
        mfsize:
        numeric

        folder:
        str.

    '''
    makedir(folder)
    minx , maxy = mf_leftupper_corner
    nlay,nrow,ncol = Kx.shape
    maxx , miny = minx+nrow*mfsize,maxy-nrow*mfsize
    xi,yi=np.meshgrid(np.linspace(minx,maxx,100),np.linspace(miny,maxy,100))
    unknow_loc = np.hstack((xi.flatten().reshape(-1,1),yi.flatten().reshape(-1,1)))
    xx,yy = np.meshgrid(np.linspace(minx,maxx,ncol,endpoint = False)+250,np.linspace(miny,maxy,nrow,endpoint = False)+250)
    K_cc_loc_df = np.vstack((xx.ravel(),yy[::-1].ravel())).T

    #input basemap
    if type(basemap) != type(None):
        ds = gdal.Open(basemap)
        width,height = ds.RasterXSize, ds.RasterYSize
        gt = ds.GetGeoTransform()
        left, bottom= gt[0] , gt[3] + width*gt[4] + height*gt[5] 
        right, top = gt[0] + width*gt[1] + height*gt[2] , gt[3] 
        basemap = plt.imread(basemap)
    count = 0
    for k in [Kx,Kz]:
        if type(k) == type(None):
            continue
        if count == 0:
            kname = 'Kh'
        else:
            kname = 'Kz'
        for i in np.arange(nlay):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            idwed_values = idw_kdtree(K_cc_loc_df,k[i,:,:].reshape(-1,1),unknow_loc)
            CS = plt.contourf(xi,yi,idwed_values.reshape(100,100),15,cmap ='Reds',alpha = 0.6,extend='both')
            plt.colorbar(CS)
            plt.title(kname+' layer %d'%(i+1))
            plt.xticks([])
            plt.yticks([])
            plt.xlim(minx,maxx)
            plt.ylim(miny,maxy)
            if type(basemap) != type(None):
                ax.imshow(basemap,extent=[left,right,bottom,top])
                plt.xlim(minx-3000,maxx+3000)
                plt.ylim(miny-3000,maxy+3000)
            plt.savefig(folder+r'\\'+kname+r' layer %d'%(i+1), psi = 300)
        count+=1
    ds = None

def EOF2mfcell_mask(EOF,Eof_grid_xy,mask_value,mfgrid_cencell_df,mf_valid_cell,plot=False,ax = None):
    '''
    Input
        EOF:
            [n x 1]. 1D numpy array.
            EOF values.
        Eof_grid_xy:
            [s x 2]. DataFrame.
            The coordinate of EOF grid.
            First column is x-coordinate.
            Second columns is y-coordinate.
        mask_value:
            Int.
            The EOF value bigger than mask_value will be masked.
        mfgrid_cencell_df:
        
        

    '''
    mfgrid_cencell_eof = idw_kdtree(Eof_grid_xy,EOF,mfgrid_cencell_df)
    masker_ = np.where(mfgrid_cencell_eof<mask_value,False,True).reshape(-1) * mf_valid_cell.reshape(-1)
    mfgrid_cencell_eof_df = pd.DataFrame(np.hstack((mfgrid_cencell_df.values,masker_.reshape(-1,1))))
    masker = masker_.reshape(46,61)
    if plot:
        plot_eof_df = mfgrid_cencell_eof_df.loc[mfgrid_cencell_eof_df.iloc[:,-1]==1,:]
        ax.scatter(plot_eof_df.iloc[:,0],plot_eof_df.iloc[:,1],c = 'white',s = 1)
    else:
        return masker

def Truecell2mfcellnum(df,two_dim=True,lay = 1 ):
    if two_dim:
        return list(zip(np.where(df)[0],np.where(df)[1]))
    else:
        return list(zip((lay-1)*np.ones(len(np.where(df)[0])).astype(int),np.where(df)[0],np.where(df)[1]))