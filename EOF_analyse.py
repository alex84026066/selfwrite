# -*- coding: utf-8 -*-
"""
Created on Tue May 26 18:57:18 2018

@author: Hua-Ting
"""
import gdal
import twd97
import os,pdb
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from stamps.stats import stl,eof
import matplotlib.dates as mdates
import matplotlib.font_manager as fm
import pandas.tseries.offsets as pto 
from stamps.general import coord2dist
from scipy.interpolate import griddata
from stamps.stest.idw import idw_kdtree
from stamps.graph.shpplot import polygonbkplot
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import shapefile as shp
import matplotlib.patheffects as path_effects
from selfwrite.general import normalize,makedir

mpl.rcParams.update({'figure.max_open_warning': 0})
fontpath='C:\\Windows\\Fonts\\msjh.ttc'
fontp = fm.FontProperties(family=u'Source Han Sans TW',fname=fontpath)

def calculate_distance(one_point,two_point):
	return np.sqrt((one_point[0]-two_point[0])**2+(one_point[1]-two_point[1])**2)

def find_nearst_point(point,target_loc_matrix):

	dis_list = []
	for i in np.arange(len(target_loc_matrix)):
		dis_list.append(calculate_distance(point,target_loc_matrix.values[i,:]))
	minloc_index = dis_list.index(np.min(dis_list))
	return target_loc_matrix.index[minloc_index]

def toTimestamp(object_index):
	count = 0
	object_index_copy = object_index.copy()
	for i in object_index:
		object_index_copy[count]  =  pd.Timestamp(i)
		count += 1
	return object_index_copy

def strtounicode(object_index):
	count = 0
	object_index_copy = object_index.values
	for i in object_index:
		object_index_copy[count]  =  i.decode('utf-8')
		count += 1
	return object_index_copy
	
def det_month(Timestamp):
	month_list=[]
	for i in Timestamp.to_pydatetime():
		month_list.append(i.month)
	return np.array(month_list)

def cut_headtail_nan(cuted_series):
	cuted_series_bool = cuted_series.isna()
	False_index = np.where((cuted_series_bool[:-1] == cuted_series_bool.values[1:]) == False)
	head_cut_point = cuted_series.index[0]
	tail_cut_point = cuted_series.index[-1]
	if np.isnan(cuted_series[0]) == True:
		head_cut_point = cuted_series.index[False_index[0].min()+1]
	if np.isnan(cuted_series[-1]) == True:
		tail_cut_point = cuted_series.index[False_index[0].max()]
	return cuted_series[head_cut_point:tail_cut_point]

def matrix_nan_info(nan_matrix):
	nan_number = nan_matrix.isna().sum()
	nan_percent = nan_number*100/len(nan_matrix)
	return nan_number,nan_percent

def idw_fill_missingdata(time_data_matrix,method = 'space',sta_loc = None,
	fill_sta_colname = 'all',nnear = 10,showpros = True):
	'''
	timeseries_matrix type must be pandas 
	and index must be Timestamp 

	Syntax
		time_data_matrix_fillna = idw_fill_missingdata(time_data_matrix,method = 'space',sta_loc = None,fill_sta_colname = 'all',nnear = 10)

	Input
	time_data_matrix:
		[t x s].DataFrame.Timeseries is used for outlier detection.
		Type of DataFrame's index should be the pandas Timestamp.
		t is the time lengh
		s is the station number
	sta_loc:
	    [s x 2]. DataFrame.
	    first column is x-coordinate
	    second column is y-cooridnate
	    s is the station number
	fill_sta_colname:
		list.
		Which col you want to fill nan. default is 'all'.
	'''
	if time_data_matrix.__class__.__name__ == 'Series':
		time_data_matrix = pd.DataFrame(time_data_matrix)
	nan_time_data_matrix = time_data_matrix.copy()
	if fill_sta_colname == 'all':
		fill_sta_colname = nan_time_data_matrix.columns
	if method =='time':
		for i in np.arange(len(time_data_matrix.T)):
			nan_time_data = nan_time_data_matrix.iloc[:,i]
			if nan_time_data.isna().sum() == len(nan_time_data):
				nan_time_data_matrix.iloc[:,i] = nan_time_data
				print ('columns%sis_all_nan'%i)
				continue
			know_index = nan_time_data[nan_time_data.isna()==False].index
			unknow_index = nan_time_data[nan_time_data.isna()==True].index
			know_value =  nan_time_data.loc[know_index]
			unknow_values = idw_kdtree(know_index.values.reshape(-1,1),know_value.values.reshape(-1,1),unknow_index.values.reshape(-1,1))
			nan_time_data.loc[unknow_index] = pd.Series(unknow_values.reshape(-1),index = unknow_index)
			nan_time_data_matrix.iloc[:,i] = nan_time_data
			if showpros:
				print ('columns%sfinish_idw'%i)
	if method == 'space':
		try:
			sta_loc.index = time_data_matrix.columns
		except AttributeError:
			print ('type of sta_loc need to be the DataFrame')
			return
		for ii in nan_time_data_matrix.index:
			fill_nan_df = nan_time_data_matrix.loc[ii,fill_sta_colname]
			if fill_nan_df.isna().sum() == 0:
				continue
			idw_array = nan_time_data_matrix.loc[ii]
			# know_loc = sta_loc.loc[idw_array[idw_array.isna()==True].index]
			# unknow_loc = sta_loc.loc[idw_array[idw_array.isna()==False].index]

			try:
				nan_time_data_matrix.loc[ii,fill_sta_colname] = idw_kdtree(sta_loc.values,idw_array.values,sta_loc.loc[fill_sta_colname].values, nnear=nnear)
			except IndexError:
				nan_time_data_matrix.loc[ii,fill_sta_colname] = idw_kdtree(sta_loc.values,idw_array.values,sta_loc.loc[fill_sta_colname].values, nnear=len(idw_array))
	return nan_time_data_matrix.loc[:,fill_sta_colname]

def matrix_plot(ploted_matrix,first_matrix_label='Raw_timeseries',second_matrix = None,second_matrix_label='Timeseries',
	show = False, folder = 'Noname',yaxisname = None,secmat_sep_plot = False,figsize  = None,mat2c = 'r'):
	'''
	Plot the input matrix. Columns is variable and row is observation. 
	If second_matrix is given. This function will plot the corresponding columns of two matrix on a figure.

	Syntax
		matrix_plot(ploted_matrix,first_matrix_label='Raw_timeseries',second_matrix = None,second_matrix_label='Timeseries',
	folder = 'Noname',yaxisname = None,secmat_sep_plot = False)

	Input
	ploted_matrix:
		[t x s].DataFrame.Timeseries is used for ploting.
		t is the time lengh
		s is the station number
	first_matrix_label:
	    str.
	    Text to use for the 1st matrix's label.
	second_matrix:
		[t x s].DataFrame.Timeseries is used for ploting.
		t is the time lengh
		s is the station number
		default is None.
	second_matrix_label:
	    str.
	    Text to use for the 2ed matrix's label.	
	folder:
		str.
		The folder that you put your plot figure.
		if assigned folder doesn't exist, this function will create one.
	yaxisname:
		str.
		Text to use for the yaxis.
	secmat_sep_plot:
		boolen.
		If second_matrix is given, you can chose whether to plot two line separately.
		default is False.
	'''
	if not show:
		makedir(folder)
	if type(ploted_matrix.columns[0]) == type('str'):
		pass
		#ploted_matrix.columns = strtounicode(ploted_matrix.columns)
	for v in np.arange(len(ploted_matrix.T)):
		if ploted_matrix.iloc[:,v].isna().sum() == len(ploted_matrix.iloc[:,v]):
			print(ploted_matrix.columns[v]+' station is all nan')
			continue
		fig, ax = plt.subplots(figsize = figsize)
		if secmat_sep_plot:
			plotdf = (pd.concat((ploted_matrix.iloc[:,[v]],second_matrix.iloc[:,[v]]),axis = 1))
			plotdf.columns = [first_matrix_label,second_matrix_label]
			ax = plotdf.plot(subplots =True,ax = ax,grid=True)
			__ = [i.legend(loc='best',prop =fontp) for i in ax]
		else:
			ax.plot(ploted_matrix.index,ploted_matrix.iloc[:,v],label = first_matrix_label)
			if second_matrix is None:
				pass
			else :
				ax.plot(second_matrix.index,second_matrix.iloc[:,v],c = mat2c,label = second_matrix_label)
			ax.legend(loc = 'best',fontsize=12)
		if yaxisname is None:
			pass
		else:
			ax = [i.set_ylabel(yaxisname) for i in ax]
		if type(ploted_matrix.columns[v]) == type('str'):
			fig.suptitle(ploted_matrix.columns[v],fontproperties=fontp,fontsize=13)
		else:
			fig.suptitle(u'{0}'.format(ploted_matrix.columns[v]),fontproperties=fontp,fontsize=13)
		plt.xlabel('date',fontsize=12)
		plt.grid()
		if show:
			plt.show()
		else:
			plt.savefig(folder+'\\Station'+str(ploted_matrix.columns[v])+'.png')
		plt.clf()
		print (str(ploted_matrix.columns[v])+'_figplot_finish')

def outlier_detection(TS_matrix,std_multiple = 3,method = 'stl',otnp = None,stlloc = None,
	stl_method_compare_surround_station = False,surround_sta_std_mul=3,detect_sta_colname = 'all',
	detect_time_period = 'all'):
	'''
	Syntax
		detected_matrix,outlier_number,outlier_percent = gw_outlier_detection(TS_matrix,std_multiple = 3)
	
	Input
		TS_matrix:
			[t x s].DataFrame.Timeseries is used for outlier detection.
			Type of DataFrame's index should be the pandas Timestamp.
			t is the time lengh
			s is the station number
			time interval may be day or hour
		
		method:
			'm_extend':
				If value over it's monthly average +/- specific multiple standard deviation 
				which is the mean of the standard deviation of month each year,judged to be the outlier. 
				For example:
				Assuming mean of 2005-5 is 8
				and the mean of standard deviation of May each year is 2 
				assuming std_multiple = 3 
				so the value if bigger than 8+2*3 or smaller than 8-2*3
				we judged to be ouotlier.
			'whole':
				used all timeseries to calculate mean and stadard deviation,
				and if data over mean +/- multiple standard deviation,
				judged to be the outlier.
			'year':
				if method = 'year',calculate mean and stadard deviation by each year.
				In specific year,if values over its mean plus/minus+/-
			'quarter':
				logic is similiar to year. judge outlier in each quarter.
			'month': 
				logic is similiar to year. judge outlier in each month.
			'stl':
				pass
			'qm_ws':
				qm_ws is mean 'quarter mean whole serise stadard deviation',
				it is use each quater mean +/- whole series multiple standard deviation as 
				outlier boundary.Out of this range we judge it as outlier.
		
		std_multiple:
			nonnegative float,It is the multiple of standard deviation 
			to be the criterion to judge outlier.
			 
		detect_sta_colname:
			list.
			Which col you want to fill nan. default is 'all'.
		detect_time_period:
			list.
			Which index you want to fill nan. default is 'all'.
			the input format equal timestamps distract values.
	Output
		detected_matrix
		[t x s].timeseries_matrix replace outlier with np.nan

	'''
	
	if detect_sta_colname == 'all':
		detect_sta_colname = TS_matrix.columns
	if detect_time_period == 'all':
		detect_time_period = TS_matrix.index
	else:
		detect_time_period = detect_time_period[0]
	timeseries_matrix = TS_matrix.loc[detect_time_period,detect_sta_colname].copy()
	data_time = timeseries_matrix.index
	if method == 'm_extend':
		timeseries_matrix = timeseries_matrix.sort_index()
		s = len(timeseries_matrix.T)
		TS_matrix_columns = timeseries_matrix.columns
		month_mean = timeseries_matrix.resample('m').mean()
		month_mean = month_mean.set_index(month_mean.index.astype('period[M]'))
		month_std = timeseries_matrix.resample('m').std()
		month_time_array = data_time.astype('period[M]')
		month_mean_matrix = month_mean.loc[month_time_array]
		month = 0
		month_std_sort_month = det_month(month_std.index)
		month_std_matrix = month_std.set_index(month_std_sort_month)
		all_month_std_mean = pd.DataFrame([])
		for j in np.arange(12):
			one_month_std_mean = month_std_matrix.loc[j+1].mean(axis = 0)
			one_month_std_mean = pd.DataFrame(one_month_std_mean.values.reshape(-1,1))
			all_month_std_mean = pd.concat([all_month_std_mean,one_month_std_mean],axis = 1)
		all_month_std_mean= all_month_std_mean.T.set_index(np.arange(1,13))
		date_time_month = det_month(data_time)
		std_matrix = all_month_std_mean.loc[date_time_month]
		judged_matrix = abs(timeseries_matrix.values-month_mean_matrix.values)/(std_multiple*std_matrix.values)
		judged_matrix = pd.DataFrame(judged_matrix,index = timeseries_matrix.index,columns = timeseries_matrix.columns)
		judged_cri = judged_matrix<1
		detected_matrix = timeseries_matrix.where(judged_cri,np.nan)
	elif method == 'whole':
		whole_series_mean_matrix = timeseries_matrix.mean(axis = 0)
		whole_series_std_matrix = timeseries_matrix.std(axis = 0)
		detected_matrix = timeseries_matrix.mask((abs(timeseries_matrix-whole_series_mean_matrix)>std_multiple*whole_series_std_matrix),np.nan)	
	elif method == 'qm_ws':
		Tmark = method[0]
		period_mean = timeseries_matrix.resample(Tmark).mean()
		period_mean = period_mean.set_index(period_mean.index.astype('period['+Tmark+']'))
		whole_series_std = timeseries_matrix.std(axis = 0)
		period_time_array = data_time.astype('period['+Tmark+']')
		period_mean_matrix = period_mean.loc[period_time_array]
		mask_matrix = (abs(timeseries_matrix-period_mean_matrix.values)-std_multiple*whole_series_std)>0
		detected_matrix = timeseries_matrix.mask(mask_matrix,np.nan)
	elif method == 'year' or method == 'quarter' or method == 'month':
		Tmark = method[0]
		period_mean = timeseries_matrix.resample(Tmark).mean()
		period_mean = period_mean.set_index(period_mean.index.astype('period['+Tmark+']'))
		period_std = timeseries_matrix.resample(Tmark).std()
		period_std = period_std.set_index(period_std.index.astype('period['+Tmark+']'))
		period_time_array = data_time.astype('period['+Tmark+']')
		period_mean_matrix = period_mean.loc[period_time_array]
		period_std_matrix = period_std.loc[period_time_array]
		detected_matrix = timeseries_matrix.mask((abs(timeseries_matrix-period_mean_matrix.values)>std_multiple*period_std_matrix.values),np.nan)
	elif method == 'stl':
		if otnp is None:
			print ('you should give a np parameter if you want to use stl method')
			return	
		residue_timeseries_matrix = stl_plot(timeseries_matrix,stl_component = {'residue'},stlns = 'per',stlnp = otnp,doplot = False,stlfolder = 'Noname')
		jugde_condition = abs(residue_timeseries_matrix-residue_timeseries_matrix.mean(axis = 0))>std_multiple*residue_timeseries_matrix.std(axis = 0)
		if stl_method_compare_surround_station== True:
			if stlloc is None:
				print ('you should give a stlloc parameter if you want to use stl method')
				return	
			jugde_condition[timeseries_matrix.isna()==True] = False
			for i in jugde_condition.columns:
				one_station = jugde_condition[i]
				need_idw_timematrix = one_station.loc[one_station==True].index
				for k in need_idw_timematrix:
					know_value = (timeseries_matrix.loc[k])[timeseries_matrix.loc[k].isna()==False]
					new_loc = stlloc.loc[know_value.index]
					know_value[i] = np.nan
					try:
						unknow_values = idw_kdtree(new_loc.values,know_value.values.reshape(-1,1),new_loc.values)
					except IndexError:
						unknow_values = idw_kdtree(new_loc.values,know_value.values.reshape(-1,1),new_loc.values,
							nnear=len(know_value.dropna()))
					raw_value = timeseries_matrix[i].loc[k]
					sta_num = np.where(know_value.isna()==True)
					if abs(raw_value-unknow_values[sta_num])<surround_sta_std_mul*know_value.std():
						jugde_condition[i].loc[k] = False
		detected_matrix = timeseries_matrix.mask(jugde_condition,np.nan)

	outlier_number = detected_matrix.isna().sum()-timeseries_matrix.isna().sum()
	outlier_percent = outlier_number/((~(timeseries_matrix.isna())).sum())
	TS_matrix.loc[detect_time_period,detect_sta_colname] = detected_matrix
	return TS_matrix,outlier_number,outlier_percent*100

def correlation_analyse(leadind_timeseries,timeseries,find_range):
	cor_list = []
	for i in np.arange(1,find_range):
		cor = np.corrcoef(leadind_timeseries[:-i].reshape(1,-1),timeseries[i:].reshape(1,-1))
		cor_list.append(cor[0,1])
	return cor_list.index(np.max(cor_list))+1,np.max(cor_list)

def STgrid_interp(know_location,timeseries_matrix,unknow_grid,
	method = 'idw',nnear = 8,xytz_fmt = True):
	'''

	Syntax
		grid_outcome = idw_STgrid_interp(know_location,timeseries_matrix,unknow_grid)

	Input
		know_location:
			[s x 2].DataFrame or numpy array.
			It is know timeseries location
			s is the station number
			first column is x-coordinate
		    second column is y-cooridnate
		timeseries_matrix:
		    [t x s]. DataFrame or numpy array.
		    know timeseries data.
		unknow_grid:
			[n x 2].DataFrame or numpy array.
			coordinate of grid which wanted interplote.
			n is the grid number
			first column is x-coordinate
		    second column is y-cooridnate
	Output
		grid_outcome.DataFrame.
			[n*t x 4].numpy array.
			first column is grid x-coordinate
		    second column is grid y-cooridnate
		    third column is time 
		    last columns is interploted timeseries
	'''
	if know_location.__class__.__name__ == 'DataFrame':
		know_location = know_location.values
	if timeseries_matrix.__class__.__name__ == 'DataFrame':
		timeseries_matrix = timeseries_matrix.values
	if unknow_grid.__class__.__name__ == 'DataFrame':
		unknow_grid = unknow_grid.values
	tlen = len(timeseries_matrix)
	grid_timeseries_matrix = np.zeros((tlen,len(unknow_grid)))
	for k in np.arange(tlen):
		know_values = timeseries_matrix[k,:]
		if method =='idw':
			grid_timeseries_matrix[k,:] = idw_kdtree(know_location,know_values,unknow_grid,nnear = nnear)
		elif method == 'nearest':
			grid_timeseries_matrix[k,:] = griddata(know_location,know_values,unknow_grid,method = 'nearest')
		print('t = %d_finish'%k)
	if xytz_fmt:
		t = np.repeat(np.arange(tlen),len(unknow_grid))+1
		s = np.tile(unknow_grid,(tlen,1))
		grid_outcome = pd.DataFrame(np.hstack((np.hstack((s,t.reshape(-1,1))),grid_timeseries_matrix.reshape(-1,1))))
		grid_outcome.columns  = ['X','Y','t','Z']
		return grid_outcome	
	else:
		return grid_timeseries_matrix

def stl_plot(timeseries_matrix,stl_component = {'seasonal','residue'},stlns = 'per',stlnp = 365,doplot = False,
	figsize = None,labelname = 'TimeSeries',yaxisname = None,stlfolder = 'Noname'):
	'''
	timeseries_matrix type must be pandas 
	and index must be Timestamp 

	'''
	sta_num = len(timeseries_matrix.T)
	t = timeseries_matrix.index
	for u in np.arange(sta_num):
		if u == 0:
			seasonal_matrix = timeseries_matrix.copy()
			trend_matrix = timeseries_matrix.copy()
			residue_matrix = timeseries_matrix.copy()
		if np.isnan(timeseries_matrix.iloc[0,u]) or np.isnan(timeseries_matrix.iloc[-1,u]):
			print ('This_station_is_begin_or_end_of_nan')
			cuted_series = cut_headtail_nan(timeseries_matrix.iloc[:,u])
			stlmatrix = stl.stl(cuted_series.values,ns = stlns,np = stlnp)
			nan_matrix = pd.DataFrame(np.full((len(timeseries_matrix),3),np.nan),index = timeseries_matrix.index)
			nan_matrix.loc[cuted_series.index.min():cuted_series.index.max()] = stlmatrix
			stlmatrix = nan_matrix.values			
		else:
			stlmatrix = stl.stl(timeseries_matrix.iloc[:,u].values,ns = stlns,np = stlnp)
		seasonal_matrix.iloc[:,u] =  stlmatrix[:,0]
		trend_matrix.iloc[:,u] =  stlmatrix[:,1]
		residue_matrix.iloc[:,u] =  stlmatrix[:,2]
		print ('station%d_finish_stl'%(u+1))
	stl_outcome_matrix = np.zeros(timeseries_matrix.shape)
	if 'seasonal' in stl_component:
		stl_outcome_matrix = stl_outcome_matrix + seasonal_matrix
	if 'trend' in stl_component:
		stl_outcome_matrix = stl_outcome_matrix + trend_matrix
	if 'residue' in stl_component:
		stl_outcome_matrix = stl_outcome_matrix + residue_matrix
	if doplot == True:
		matrix_plot(stl_outcome_matrix,folder = stlfolder,first_matrix_label = labelname,yaxisname=yaxisname,figsize = figsize)
	return stl_outcome_matrix

def change_garbage_format(garbage_format_matrix,starttime):
	'''
	first column is station name
	second column is x-coordinate and third column is y-coordinate
	fourth column is first day in each month
	fifth column is second day in each month 
	and so on  
	time freq is daily
	'''
	new_format_waterlevel_matirx = pd.DataFrame([])
	sta_name_xy = garbage_format_matrix.iloc[:,0:3].drop_duplicates()
	sta_name = garbage_format_matrix.iloc[:,0].drop_duplicates()
	month_num = len(garbage_format_matrix)/len(sta_name)
	for k in sta_name:
		one_sta_wl_matrix = garbage_format_matrix[garbage_format_matrix.iloc[:,0]==k].iloc[:,3:]
		one_sta_waterlevel_array = pd.DataFrame([])
		for i in np.arange(month_num):
			start_time = starttime + i*pto.DateOffset(months = 1)
			end_time = start_time + pto.DateOffset(months = 1) - pto.DateOffset(days = 1)
			DataFrameindex = pd.date_range(start_time,end_time)
			import pdb
			pdb.set_trace()
			one_month_data = pd.DataFrame(one_sta_wl_matrix.iloc[i,:len(DataFrameindex)].values.reshape(-1,1),index = DataFrameindex)
			one_sta_waterlevel_array = pd.concat((one_sta_waterlevel_array,one_month_data),axis = 0)
		new_format_waterlevel_matirx = pd.concat((new_format_waterlevel_matirx,one_sta_waterlevel_array),axis = 1)
	return new_format_waterlevel_matirx

def doEOF_plot(plot_value,plot_loc,StaPointPlt = True,plot_num=None,rota = None,norm = None,folder = 'Noname',plot_exper_bar_num = 10,eof_basemap = None,
	eof_plot_extent=None,EC_xtick_show_freq=None,EC_xtick_show_para = [None,None],EC_xtick_rotation = None,shpfile1=None,shpfile2 = None,
	shp_para = [['black',2],['black',0.3]],raindata=None,rainloc=None,figsize = [None,None,None],figtitlefs = [None,None,None],ShowPros = True):
	
	'''
	This function do three things
	1.EOF analyse
	2.Plot the EOF outcome and EC
	3.If you have rainfall data,it can compare EC and rainfall that near the significant eof point

	Input
		plot_value:
			[t x s].DataFrame.
			Type of DataFrame's index should be the pandas Timestamp.
			t is the time lengh
			s is the station number
		plot_loc:
			[s x 2].DataFrame
		    first column is x-coordinate
		    second column is y-cooridnate
		    s is the station number		
		rota:
			bool or None.
			If rotate EOF. Default will do both rotation eof and unrotation eof.
		norm:
			bool or None.
			If it is true, then all time series are normalized by 
			their standard deviation before EOFs are computed.
			Default will do both normalize and non-normalize.
		shpfile1:
			Path.
			This shpfile will be the basemap of eof plot.
			This parameter usually input the county boundary 
		plot_exper_bar_num:
			Int.
			How many experience variation you want to show.
		eof_basemap:
			Path.
			The basemap used to eof plot.
			Not necessary.
			This parameter usually input the .Tif file of research area. 
		eof_plot_extent:
			[minx,maxx,miny,maxy].List of eof countour plot boundary
		plot_num:
			int.
			How many component of eof you want to plot.
			If None, all component will be plotted.
		EC_xtick_show_freq:
			string.
			The time freqency you want to show in x-axis in EC plot.
			You only can input 'm' or 'd'.
			ref: https://matplotlib.org/3.1.3/api/dates_api.html
		EC_xtick_show_para:
			List.
			Only use it when EC_xtick_show_freq != Not.
			First element:
				Xticks's Locator which is matplotlib.dates object
				matplotlib.dates object ref: https://matplotlib.org/3.1.3/api/dates_api.html

			Second element:
				String.
				The datetime index showing format.
				eg “%d/%m/%Y”, note that “%f” will parse all the way up to nanoseconds. 
				See strftime documentation for more information on choices: 
				https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior.

		EC_xtick_rotation:
			Set the rotation of the xticks.
			You can input: 
				1.angle in degrees, like 45
				2.'vertical'
				3.'horizontal'
		shpfile2:
			Path.
			The second shafile you want to use as basemap.
		shp_para:
			two sublist in one list.
			First sublist please input [shp1_color,shp1_linewidth]
			Second sublist please input [shp2_color,shp2_linewidth]
			Exsample. => [['black',2],['black',0.3]]
		folder:
			Path.
			The path you want to save the ploted figure.
		raindata:
			[t x s].DataFrame.
			If raindata and rainloc is given, you will plot the figure that compare Rainfall and EC.
			You need to input rainfall data.
			t is the time lengh
			s is the station number
		rainloc:
			[s x 2].DataFrame.
			If raindata and rainloc is given, you will plot the figure that compare Rainfall and EC.
			You need to input rainfall station xy-coordinate.
			s is the station number
		figsize:
			list.
			first element is the EOF plot figsize. Default is None.
			second element is the EC plot figsize. Default is None.
			third element is the EC compare with rainfall plot figsize. Default is None.
			Example. =>  [(8,8),(12,6),(12,6)]
		figtitlefs:
			list.
			first element is the EOF plot title fontsize.Default is None.
			second element is the EC plot title fontsize.Default is None.
			third element is the EC compare with rainfall plot title fontsize. Default is None.
		
	'''
	def shafileplot(input_ax,shpfile_path,color,linewidth):
		sf = shp.Reader(shpfile_path,encoding='latin1')
		sf.decoding = 'latin1'
		for shape in sf.shapeRecords():
			for i in range(len(shape.shape.parts)):
				i_start = shape.shape.parts[i]
				if i==len(shape.shape.parts)-1:
					i_end = len(shape.shape.points)
				else:
					i_end = shape.shape.parts[i+1]
				x = [i[0] for i in shape.shape.points[i_start:i_end]]
				y = [i[1] for i in shape.shape.points[i_start:i_end]]
				input_ax.plot(x,y,lw = linewidth,c = color)

	def eof_and_ec_plot(eof_matrix,ec_matrix,exper_x,exper_y,savfolder):
		for k in np.arange(plot_num):
			#plot_EC
			fig_ec = plt.figure(figsize  = figsize[1])
			ax_ec = fig_ec.add_subplot(111) 
			plt.plot(plot_x,ec_matrix[:,k],label = 'EC%d'%(k+1))
			if EC_xtick_show_freq != None:
				EC_xtick_show_freq_default_dict = {'m':[mdates.MonthLocator(),'%Y-%m'],'d':[mdates.DayLocator(interval=3),'%m-%d']}
				locator = EC_xtick_show_freq_default_dict[EC_xtick_show_freq][0]
				formatter = EC_xtick_show_freq_default_dict[EC_xtick_show_freq][1]
				if EC_xtick_show_para[0] != None:
					locator = EC_xtick_show_para[0]
				if EC_xtick_show_para[1] != None:
					formatter = EC_xtick_show_para[1]
				ax_ec.set_xticklabels(plot_x,rotation=EC_xtick_rotation)
				ax_ec.xaxis.set_major_locator(locator)
				ax_ec.xaxis.set_major_formatter(mdates.DateFormatter(formatter))
			plt.xticks(rotation=EC_xtick_rotation)
			plt.legend(loc = 'best')
			plt.tight_layout()
			plt.savefig(folder+savfolder+r'\EC%d'%(k+1))
			plt.clf()
			#plot_EOF
			fig_eof = plt.figure(figsize = figsize[0])
			ax_eof = fig_eof.add_subplot(111)
			if shpfile1 is not None:
				shafileplot(ax_eof,shpfile1,color = shp_para[0][0],linewidth = shp_para[0][1])
			if shpfile2 is not None:
				shafileplot(ax_eof,shpfile2,color = shp_para[1][0],linewidth = shp_para[1][1])

			idwed_values = idw_kdtree(plot_loc.values,eof_matrix[:,k],unknow_loc,nnear =5 )
			CS = plt.contourf(xi,yi,idwed_values.reshape(100,100),15,cmap ='Reds',alpha = 0.6,extend='both')
			if StaPointPlt:
				plt.scatter(plot_loc.iloc[:,0],plot_loc.iloc[:,1],marker='*',s=50,c = 'blue')	
				for j in np.arange(len(plot_loc)):
					plt.annotate(plot_loc.index[j],(plot_loc.values[j,0]-800,plot_loc.values[j,1]+800),weight="bold",fontproperties=fontp)
			plt.colorbar(CS)
			plt.title('EOF%d'%(k+1),fontsize=figtitlefs[0])
			plt.xticks([])
			plt.yticks([])
			plt.xlim(minx,maxx)
			plt.ylim(miny,maxy)
			if eof_basemap is not None:
				ax_eof.imshow(Eof_basemap,extent=[left,right,bottom,top])
				plt.xlim(minx-3000,maxx+3000)
				plt.ylim(miny-3000,maxy+3000)
			plt.tight_layout()
			plt.savefig(folder+savfolder+r'\EOF%d'%(k+1), psi = 300)
			plt.clf()
			if ShowPros:
				print ('finish_'+savfolder[1:]+'_EOF%d_plot'%(k+1))
		plt.figure()
		plt.xlabel('EOF')
		plt.title(u'解釋變異量',fontproperties=fontp,fontsize=figtitlefs[2] )
		plt.ylabel('%')
		plt.bar(exper_x,100*exper_y,0.8)
		plt.xticks(exper_x)
		plt.tight_layout()
		plt.savefig(folder+savfolder+'\\explain_per.png')
		plt.clf()
		if ShowPros:
			print ('finish_'+savfolder[1:]+'_exper_bar_plot'%(k+1))

	def EC_with_nearst_raindata(EOF_matrix,EC_matrix,ec_rain_folder):
		makedir(ec_rain_folder)
		for i in np.arange(plot_num):
			[[max_eof_index]] = np.where(EOF_matrix[:,i] == np.max(EOF_matrix[:,i]))
			sig_eof_point = plot_loc.iloc[max_eof_index,:]
			sig_eof_name = sig_eof_point.index
			nearst_rainfall_name = find_nearst_point(sig_eof_point,rainloc)
			fig2 = plt.figure(figsize = figsize[-1])
			ax2 = fig2.add_subplot(1,1,1) 
			plt.plot(raindata[nearst_rainfall_name].index,normalize(raindata[nearst_rainfall_name].values))
			plt.plot(plot_x,normalize(EC_matrix[:,i]),label = 'EOF%d'%(i+1))
			if EC_xtick_show_freq == 'm':
				ax2.set_xticklabels(plot_x,rotation=EC_xtick_rotation)
				ax2.xaxis.set_major_locator(mdates.MonthLocator())
				ax2.xaxis.set_major_formatter(mdates.DateFormatter(EC_xtick_show_format))
			plt.xticks(rotation=EC_xtick_rotation)
			plt.legend(loc='best')
			plt.title(u'{0}'.format(nearst_rainfall_name),fontproperties=fontp,fontsize=figtitlefs[1])
			plt.tight_layout()
			plt.savefig(ec_rain_folder+'\\EC%d'%(i+1))
			plt.clf()

	#set ploting figure boundary
	if eof_plot_extent == None:
		minx,maxx = plot_loc.iloc[:,0].min()-2000,plot_loc.iloc[:,0].max()+2000
		miny,maxy = plot_loc.iloc[:,1].min()-2000,plot_loc.iloc[:,1].max()+2000
	else:
		minx,maxx,miny,maxy = eof_plot_extent
	#input basemap
	if eof_basemap is not None:
		ds = gdal.Open(eof_basemap)
		width = ds.RasterXSize
		height = ds.RasterYSize
		gt = ds.GetGeoTransform()
		left = gt[0]
		bottom = gt[3] + width*gt[4] + height*gt[5] 
		right = gt[0] + width*gt[1] + height*gt[2]
		top = gt[3]
		Eof_basemap = plt.imread(eof_basemap)

	#make meshgrid
	xx=np.linspace(minx,maxx,100)
	yy=np.linspace(miny,maxy,100)
	xi, yi=np.meshgrid(xx,yy)
	unknow_loc = np.hstack((xi.flatten().reshape(-1,1),yi.flatten().reshape(-1,1)))

	#Define if do rotation eof and normalize eof
	rotaYN,normYN = np.array([0,0]),np.array([0,0])

	rota = [0,1] if rota is None else int(rota)
	norm = [0,1] if norm is None else int(norm)
	rotaYN[rota],normYN[norm]= 1,1
	eofid = np.dot(rotaYN.reshape(-1,1),normYN.reshape(1,-1)).ravel()
	doeof = np.array([[0,0],[0,1],[1,0],[1,1]])[eofid.astype(bool),:]  #[0,1] means no rotation but normalize, and so on
	#create folder
	makedir(folder)
	x = range(1,plot_exper_bar_num+1)

	#EOF analyse
	if plot_num is None:
		plot_num = len(plot_loc)
	plot_x = plot_value.index
	for RotaEof,NormEof in doeof:
		sondir = 'eof_norm'+str(NormEof)
		if RotaEof:
			eigvals, sr_lams, EOFs, ECs, sr_explained_variance = eof.sreof(plot_value.values,normalized = NormEof)
			sondir = 'sr'+sondir
		else:
			eigvals, lamda , PC, EOFs, ECs, error, norms,explained_variance = eof.eof(plot_value.values,normalized = NormEof)

		#create folder
		savedir = os.path.join(folder,sondir)
		makedir(savedir)

		#plot eof and ec
		eof_and_ec_plot(EOFs,ECs,x,eigvals[:len(x)]/eigvals.sum(),savfolder='\\'+sondir)

		#compare rainfall and ec
		if (raindata is not None) and (rainloc is not None):
			if (rainloc is None):
				print('You should input Rainfall station coordinate')
				break
			EC_with_nearst_raindata(EOFs,ECs,ec_rain_folder = os.path.join(savedir,'plot_with_nearest_rainsta'))

def Dupca_value_dealing(grid_s,z,method = 'mean'):
	'''
	Deal with data in the same coordinate
	Input
		grid_s: coordinate
			[n x d1].array.
			n is the data amount
			d is the coordinate dim
		z: value
			[n x d2].array.
			n is the data amount
			d is the z dim

		method: the method to deal duplicates data
	'''

	if z.ndim == 1:
		z = z.reshape(-1,1)
	df = pd.DataFrame(grid_s)
	unique_coor = grid_s[~df.duplicated(keep=False),:]
	unique_value = z[~df.duplicated(keep=False),:]

	dup_data = grid_s[df.duplicated(keep=False),:]
	dup_data_values = z[df.duplicated(keep=False),:]
	uni_coorindup_data= df.loc[df.duplicated(keep='first'),:].values
	_coor = np.zeros((uni_coorindup_data.shape))
	_value = np.zeros((uni_coorindup_data.shape[0],z.shape[1]))
	for i in np.arange(len(uni_coorindup_data)):
		select_series = (dup_data==uni_coorindup_data[i,:]).sum(axis=1) == grid_s.shape[1]
		dup_coor_value_mean =  np.mean(dup_data_values[select_series,:])
		_coor[i,:] = uni_coorindup_data[i,:]
		_value[i,:] = dup_coor_value_mean
	unique_grid_s = np.vstack((unique_coor,_coor))
	unique_values = np.vstack((unique_value,_value))

	return unique_grid_s,unique_values

def Find_Tseries_continueNan(Tmatrix):
	'''
	Find Timeseries matrix amount of continued Nan.
	timeseries_matrix type must be pandas.
	Input
	Tmatrix:
		[t x s].DataFrame.
		t is the time lengh
		s is the station number

	'''
	nan_number,nan_percent = matrix_nan_info(Tmatrix)
	colname = Tmatrix.columns
	indexs = Tmatrix.index
	for i in colname:
		sta_name = i+'Station'
		print(sta_name)
		ust = Tmatrix[i]
		count = 0
		before_value = ust[0]
		max_nan_c = 0
		for t in ust:
			if np.isnan(t):
				count = count+1
			else:
				if np.isnan(before_value):
					if count>max_nan_c:
						max_nan_c = count
						print(max_nan_c)
					count = 0
				else:
					continue
			before_value = t
		print(i+'_Nan_number =',nan_number[i])
		print(i+'_Nan_percent =',nan_percent[i])
		print(i+'_Max_continue_nan_amount =',max_nan_c)
		print('------------------------------')

def Rain2Inflitration(Rain,CN_value):
	'''
	Use SCS method to calculate inflitation

	Syntax
		Inflitration = Rain2Inflitration(Rain,CN))

	Input
	Rain:
		[t x n]. Numpy array or DataFrame or Series.Rain data is used for inflitration calcuation.
		t is the time lengh
		n is the station number
		Unit must be mm.
	CN:
	    [n x 1].Int or 1D Numpy array or list. 
	    CN values used for SCS CN curve method.
		Each rainfall station's CN value.
		n is the station number
	'''
	Rain_Inputclasstype = Rain.__class__.__name__
	if (CN_value.__class__.__name__ == 'int') or  (CN_value.__class__.__name__ =='float'):
		CN_value = [CN_value]
	
	if Rain_Inputclasstype == 'DataFrame':
		Rindex = Rain.index
		Rcolname = Rain.columns
		Rain = Rain.values
	
	if Rain_Inputclasstype == 'Series':
		Rindex = Rain.index
		Rain = Rain.values

	station_num = len(CN_value)
	Rain = Rain.reshape(-1,station_num)
	Inflitration = Rain.copy()
	for i in np.arange(station_num):
		rainfall = Rain[:,i]
		CN = CN_value[i]
		S_20 = (1000/CN -10)
		S_5 = 25.4*1.33*S_20**1.15
		Ia = 0.05*S_5
		Pe = rainfall-Ia
		F = np.where(Pe<0,rainfall,S_5*(rainfall-0.05*S_5)/(rainfall+0.95*S_5))
		Inflitration[:,i] = F

	if Rain_Inputclasstype == 'DataFrame':
		Inflitration = pd.DataFrame(Inflitration,index=Rindex,columns = Rcolname )
	if Rain_Inputclasstype == 'Series':
		Inflitration = pd.Series(Inflitration.reshape(-1))
		Inflitration.index = Rindex
	return Inflitration