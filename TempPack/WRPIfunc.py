import geopandas
from selfwrite.graph import addSHP
import matplotlib.patheffects as pe
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
from stamps.stats.eof import eof,sreof
from selfwrite.EOF_analyse import doEOF_plot,matrix_plot
import matplotlib.dates as md 
from selfwrite.general import makedir,normalize,RW_pkl,find_nearest_point,nearestneighbor
from sklearn.datasets import load_digits
from sklearn.decomposition import FastICA
from stamps.stats import stl 
xfmt = md.DateFormatter('%m/%d')
from scipy.optimize import minimize
from scipy import signal
from PyEMD import EEMD,Visualisation
import seaborn as sns
import pyarrow.parquet as pq
import gc,pywt,pdb,scipy
from statsmodels.robust import mad
from scipy.signal import butter
import matplotlib.font_manager as fm
fontpath='C:\\Windows\\Fonts\\msjh.ttc'
fontp = fm.FontProperties(family=u'Source Han Sans TW',fname=fontpath)
import shapefile as shp
import gdal,os
from scipy.signal import argrelextrema
from selfwrite.graph import AddBasemap  
from scipy.interpolate import griddata
from selfwrite.general import nearestneighbor

def tsaddrain(ax,eof,EOFloc,rain,rainloc,tindex = None):
    sigeofloc = EOFloc[eof==eof.max()]
    rainidx,_ = find_nearest_point(sigeofloc,rainloc)
    rainest_ts = rain[str(rainidx)]
    if tindex is not None:
        tidx = tindex
    else:
        tidx = np.arange(len(rainest_ts))
    ax.plot(tidx,normalize(rainest_ts))
    return ax

def ts_spectrum(plt_ts,EOF = None,EOFloc = None,rain = None,rainloc = None
    ,m = None,show = True,savedir = None,Tname = 'Ts',spectrumplt = True,
    freqtable = True,freqWtable=True,tindex = None,addshp = None,fsufix = ''):
    '''
    rain should be dataframe.
    rainloc should be numpy array.


    '''
    if m is not None:
        plt_ts = plt_ts[:,:m]
    pltrow,pltcol = 1,3
    if EOF is not None:
        pltcol+=1
    if spectrumplt:
        pltcol+=1
    for i in np.arange(len(plt_ts.T)):
        pltorder = 0
        fig = plt.figure(figsize = (pltcol*3,3))
        spectrum ,freqs,lines  = plt.magnitude_spectrum(plt_ts[:,i],Fs=1)
        if spectrumplt:
            ssax = plt.subplot2grid((pltrow,pltcol),(0,pltorder),colspan = 1)
            ssax.add_line(lines)
            pltorder+=1
        freqs_df = (pd.DataFrame(freqs,index = spectrum,columns = ['freqs'])).sort_index(axis= 0,ascending=False)
        freqs_df['period'] = 1/freqs_df['freqs']
        ecax = plt.subplot2grid((pltrow,pltcol),(0,pltorder),colspan = 3)
        pltorder+=3
        if tindex is not None:
            tidx = tindex
        else:
            tidx = np.arange(len(plt_ts))
        if rain is not None:
            ecax = tsaddrain(ecax,EOF[:,i],EOFloc,rain,rainloc)
            ecax.plot(tidx,normalize(plt_ts[:,i]))
        else:
            ecax.plot(tidx,plt_ts[:,i])
        ecax.set_title(Tname+'%d'%(i+1))
        ecax.grid()
        if (freqtable|freqWtable):
            ecax.set_xticks([])
        tabel_text = []
        if freqtable:
            cell_text1 = np.round(freqs_df['period'].values[:10],2).astype(str).reshape(1,-1)
            tabel_text.append(cell_text1.tolist()[0])
        if freqWtable:
            ct_r2 = (100*np.array(freqs_df.index)/spectrum.sum()).ravel()
            cell_text2 = np.round(ct_r2[:10],2).astype(str).reshape(1,-1)
            tabel_text.append(cell_text2.tolist()[0])
        if freqtable or freqWtable:
            the_table = plt.table(cellText=tabel_text,cellLoc = 'center')   
            the_table.scale(1,2)
            the_table.auto_set_font_size(False)
            the_table.set_fontsize(10)
        if EOF is not None:
            eofax = plt.subplot2grid((pltrow,pltcol),(0,pltorder),colspan = 1)
            eofsca = eofax.scatter(EOFloc[:,0].ravel(),EOFloc[:,1].ravel(),c = EOF[:,i],cmap='hot_r',marker = 's')
            eofax.set_title('EOF%d'%(i+1))
            eofax.set_xticks([])
            eofax.set_yticks([])
            if addshp is not None:
                eofax = addSHP(addshp,ax = eofax,lw = 1,c= 'k')
            eofax.set_aspect('equal', 'box')
            fig.colorbar(eofsca,ax= eofax)
        plt.tight_layout(w_pad=1)
        if show:
            plt.show()
        else:
            makedir(savedir)
            plt.savefig(savedir+'/'+Tname+'%d_sp'%(i+1)+fsufix+'.png',bbox_inches = 'tight')
## Low pass filter
def ExtractnpWL(dataT,Ext_T,*args):
    oclist =[]
    for num,WL in enumerate(args):
        WL_oc = pd.DataFrame(WL,index = dataT).loc[Ext_T]
        if num==0:
            Ext_T_oc = WL_oc.index
        oclist.append(WL_oc.values)
    oclist.append(Ext_T_oc)
    return oclist
          
def lowpassfilter(signal, thresh = 0.63, wavelet="db4"):
    thresh = thresh*np.nanmax(signal)
    coeff = pywt.wavedec(signal, wavelet, mode="per")
    coeff_raw = coeff.copy()
    coeff[1:] = (pywt.threshold(i, value=thresh, mode="soft" ) for i in coeff[1:])
    reconstructed_signal = pywt.waverec(coeff, wavelet, mode="per" )
    return reconstructed_signal

def MatLPassFilter(Z,dataT,Ext_T,thresh = 0.4, wavelet="db8"):
    Z_HP = Z.copy()
    Z_LP = Z.copy()
    for i in np.arange(len(Z.T)):
        sig_wl = Z[:,i].copy()
        rec = lowpassfilter(sig_wl,thresh, wavelet)
        Z_LP[:,i] = rec
        Z_HP[:,i] = sig_wl-rec
    Z_HP,Z_LP,newT = ExtractnpWL(dataT,Ext_T,Z_HP,Z_LP)
    return Z_HP,Z_LP

def MAdetrend(Z,dataT,Ext_T,width = 24*3):
    '''
    Z should be numpy array
    '''
    Z_HP = Z.copy()
    Z_LP = Z.copy()
    for i in np.arange(len(Z.T)):
        sig_wl = Z[:,i].copy()
        rec = pd.DataFrame(sig_wl.reshape(-1,1))
        rec = rec.rolling(window=width,center=True).mean().values.ravel()
        Z_LP[:,i] = rec
        Z_HP[:,i] = sig_wl-rec
    Z_HP,Z_LP,newt = ExtractnpWL(dataT,Ext_T,Z_HP,Z_LP)
    return Z_HP,Z_LP,newt

def MatLPFplot(Z_HP,Z_LP,plotx,pltnum = [0],staName = None):
    for signum in pltnum:
        if staName is not None:
            print(staName[signum])
        Z = Z_HP+Z_LP
        fig, ax = plt.subplots(figsize=(12,2))
        ax.plot(plotx,Z[:,signum],color="b", alpha=0.5, label='original signal')
        ax.legend()
        ax.set_title('Raw Timeseries', fontsize=14)
        plt.show()
        
        
        fig, ax = plt.subplots(figsize=(12,2))
        ax.plot(plotx,Z[:,signum],color="b", alpha=0.5, label='original signal')
        ax.plot(plotx,Z_LP[:,signum], 'k', label='DWT smoothing', linewidth=2)
        ax.legend()
        ax.set_title('Seperate High Frequency with DWT', fontsize=14)
        ax.set_ylabel('Signal Amplitude', fontsize=12)
        plt.grid()
        plt.show()
        
        fig, ax = plt.subplots(figsize=(12,2))
        ax.plot(plotx,Z_HP[:,signum], 'k', label='Filtered signal', linewidth=2)
        ax.set_title('High Frequency Signal', fontsize=14)
        ax.set_ylabel('Signal Amplitude', fontsize=12)
        plt.grid()
        plt.show()

# find singal pumping patter
def ThdefSigP(difds,th_D,th_L):
    pthod = pd.DataFrame(difds[difds<0]).quantile(th_D)[0]
    rchthod = pd.DataFrame(difds[~(difds<0)]).quantile(1-th_L)[0]
    dift = np.arange(len(difds))
    pumppoint = dift[difds<pthod]
    rchpoint  = dift[difds>rchthod]  
    return pumppoint,rchpoint

def SltConPoint(Pseries,continum=2):
    Pseries_selet = Pseries.copy()
    for i in Pseries:
        juglist = []
        for j in np.arange(continum):
            juglist.append((i+j) in Pseries)
        if np.array(juglist).sum()!=continum:
            Pseriest_selet = np.setdiff1d(Pseries_selet,np.array(i))    
    return Pseriest_selet

def FindfPRpoint(pumppoint,rchpoint,pumppoint_selet,rchpoint_selet):
    fpumppoint = pumppoint_selet.copy()
    frchpoint = rchpoint_selet.copy()
    p_ind = pumppoint_selet.tolist().copy()
    for i in rchpoint_selet:
        p_ind.append(i)
    p_ind.sort()
    for n,i in enumerate(p_ind):
        if n==0:
            if i in pumppoint:
                jud = -1
            if i in rchpoint:
                jud = 1
        else:
            if i in pumppoint:
                postjud = -1
            if i in rchpoint:
                postjud = 1
            if jud==postjud:
                if jud==1:
                    frchpoint = np.setdiff1d(frchpoint,np.array(i))
                if jud==-1:
                    fpumppoint = np.setdiff1d(fpumppoint,np.array(i))
            else:
                jud = postjud
    return fpumppoint,frchpoint

def JudifExtHL(ts,Ppoint,Rpoint,TH_H):
    if len(Ppoint)>len(Rpoint):
        PRpair = zip(Ppoint[:-1],Rpoint)
    elif len(Ppoint)<len(Rpoint):
        PRpair = zip(Ppoint,Rpoint[1:])
    elif len(Ppoint)==len(Rpoint):
        if Rpoint[0]<Ppoint[0]:
            PRpair = zip(Ppoint[:-1],Rpoint[1:])
        elif Rpoint[0]>Ppoint[0]:
            PRpair = zip(Ppoint,Rpoint)
    pos_thod = pd.DataFrame(ts).quantile(1-TH_H)[0]
    nag_thod = pd.DataFrame(ts).quantile(TH_H)[0]
    Ppoint_slt = Ppoint[(ts[Ppoint]>pos_thod)]
    Rpoint_slt = Rpoint[(ts[Rpoint]<nag_thod)]
    keep_PR_ = []
    for i in PRpair:
        for j in Ppoint_slt:
            if j in i:
                keep_PR_.append(i)
    keep_PR = []
    for i in keep_PR_:
        for j in Rpoint_slt:
            if j in i:
                keep_PR.append(i)
    keep_PR = np.array(keep_PR)
    Ppoint_kp,Rpoint_kp = keep_PR[:,0],keep_PR[:,1]
    return Ppoint_slt,Rpoint_slt,Ppoint_kp,Rpoint_kp

def makePpattern(fpumppoint,frchpoint,ts):
    p_pattern = np.ones(len(ts))
    for i in np.arange(len(ts)):
        if i==0:
            if (fpumppoint[0] < frchpoint[0]):
                state = -1
            else:
                state = 0
            p_pattern[i] = state
            continue
        if i in fpumppoint:
            state = -1
        elif i in frchpoint:
            state = 0
        p_pattern[i] = state
    return p_patter

# Deal with data(singal well)
def LoadWellData(layer):
    data = np.load('./fill_na_station_normalized_2015_2017_stz_format.npz')
    stainfo = pd.read_pickle('./info_processed_data_2008_2017_mds.xz')
    Zall = pd.DataFrame(data['Zall'].T,index = data['tME'])
    Ztr = pd.DataFrame(data['Ztr'].T,index = data['tME'])
    Z = (Zall+Ztr)
    Z_L = Z.loc[:,(stainfo['GroundwaterLayerCode'] == str(layer)).values]
    XY = stainfo[['x97', 'y97']][stainfo['GroundwaterLayerCode'] == str(layer)]
    name = stainfo[['WellName']][stainfo['GroundwaterLayerCode'] == str(layer)]
    Z_L.columns = XY.index
    return Z_L,XY,name



### for grid waterlevel
def MatrixEEMD(X,m=None,trial = 500,noise_width = 0.1,seed = 12345,clipbound = [None,None],boolclip = None,showpros = True):
    '''
    X should be column-wise 2D array
    '''
    ## Do EEMD
    from PyEMD import EMD, Visualisation, EEMD, CEEMDAN
    eemd = EEMD()
    vis = Visualisation()
    eemd.trails = trial
    eemd.noise_width=noise_width
    eemd.noise_seed(seed)
    if m is None:
        m = X.shape[1]
    X = X[:,:m]
    imfslst=[]
    refslst=[]
    inst_freq_lst=[]
    t=np.linspace(1,len(X),num=len(X),endpoint=True)

    for i in np.arange(m):
        if showpros:
            print(str(i+1)+'/'+str(X.shape[1]))
        E_coms = eemd(X[:,i], t)
        E_imfs, E_res2 = E_coms[1:][:,clipbound[0]:clipbound[-1]], E_coms[0][clipbound[0]:clipbound[-1]]
        if boolclip is not None:
            E_imfs, E_res2 = E_imfs[:,boolclip], E_res2[boolclip]
        inst_freq=vis._calc_inst_freq(E_imfs,t,order=False,alpha=None)
        imfslst.append(E_imfs)
        refslst.append(E_res2)
        inst_freq_lst.append(inst_freq)
        
    return imfslst,refslst,inst_freq_lst

def LoadGridData(Y,M,Lay):
    GridWLdatadir = r'Z:\@LH-STEM.LAB\61\ksj-1000001\WorkingSpace\GWX\FillNaFormal109'
    filename = f'fill_na_grid_normalized_{Y}{str(M).zfill(2)}_{Y}{str(M).zfill(2)}_stz_format_layer_{Lay}.npz'
    da = np.load(os.path.join(GridWLdatadir,filename))
    Z = (da['Zk']+da['Ztr']).T
    t,xy = da['tME'],da['cMS']
    return Z,t,xy

def LoadGridDetrendData(Y,M,Lay,DTbuf = 24,getdatabf= None):
    if M==1:
        Llist = [[Y-1,12],[Y,M],[Y,M+1]]
    elif M==12:
        Llist = [[Y,M-1],[Y,M],[Y+1,1]]
    else:
        Llist = [[Y,M-1],[Y,M],[Y,M+1]]

    daB,t1,loc = LoadGridData(Llist[0][0],Llist[0][1],Lay)
    da,t2,loc = LoadGridData(Llist[1][0],Llist[1][1],Lay)
    daA,t3,loc = LoadGridData(Llist[2][0],Llist[2][1],Lay)
    Z = np.vstack((daB,da,daA))
    tindex = pd.date_range(t1[0],t3[-1],freq = 'H')
    if getdatabf is not None:
        getdataTs = np.where(tindex==t2[0])[0][0]-getdatabf
        getdataTe = np.where(tindex==t2[-1])[0][0]+getdatabf
        getdata_range = tindex[getdataTs:getdataTe]
    else:
        getdata_range = t2
    GridZ_HP,GridZ_LP,newt = MAdetrend(Z[24*15:-24*15],tindex[24*15:-24*15],getdata_range,DTbuf)
    return GridZ_HP,GridZ_LP,newt


def Listar2ar(keepECimf):
    '''
    output is columnwise.
    '''
    for n,i in enumerate(keepECimf):
        if n==0:
            wholear = i.reshape(-1,1)
            continue
        wholear = np.hstack((wholear,i.reshape(-1,1)))
    return wholear

def ECimfsPlot(ECs,imfslst,inst_freq_lst,ECnum):
    ECnum = ECnum-1
    EC = ECs[:,ECnum]
    imfs = imfslst[ECnum]
    for i in np.arange(len(imfs)):
        period = 1/(inst_freq_lst[ECnum][i].mean()*24)
        print('std =',np.round(imfs[i].std(),3))
        print('period=',np.round(period,3))
        plt.figure(figsize = (12,2))
        plt.plot(EC,label = 'EC%s'%str(ECnum+1))
        plt.plot(imfs[i],label = 'imf')
        plt.title('imf%s'%str(i+1))
        plt.legend(loc = 'best')
        plt.grid()
        plt.show() 

## use 

def makeAPumpTs(ts,ChkStartDrop = True,ChkEndDrop = True):
    '''
    ChkStartDrop: Bool. If true. This function will check if 
    timeseries begin with decreasing values first, if yes, 
    consider it is pumping.
    
    ChkEndDrop: Bool. If true. This function will check if 
    end of the timeseries still decreasing first, if yes, 
    Consider it is rebound point.
    '''
    def findDRpoint(ts):
        tsdiff1 = np.diff(ts)
        Crs0p = np.where(np.diff(np.sign(tsdiff1)))[0]+1
        if Crs0p.size==0:
            return np.array([]),np.array([])

        if tsdiff1[Crs0p[0]]<0:
            Dpoint = Crs0p[::2]
            Rpoint = Crs0p[1:][::2]
        else:
            Dpoint = Crs0p[1:][::2]
            Rpoint = Crs0p[::2]
        return Dpoint,Rpoint

    ts = ts.ravel()
    Ppoint,Rpoint = findDRpoint(ts)
    if ChkStartDrop:
        StartWithDrop = False
        if (ts[1]<ts[0]):
            Ppoint = np.hstack((np.array([0]),Ppoint))
            StartWithDrop = True
    if ChkEndDrop:
        if (ts[-2]>ts[-1]):
            Rpoint = np.hstack((Rpoint,np.array([len(ts)-1])))
    if ((Ppoint.size==0) or (Rpoint.size==0) or (len(Rpoint)+len(Ppoint)==1)):
        PRpair_ar =Pamount=Pseries = np.zeros(ts.ravel().shape)
        return PRpair_ar,Pamount,Pseries,Ppoint,Rpoint
    if len(Ppoint)>len(Rpoint):
        PRpair = zip(Ppoint[:-1],Rpoint)
    elif len(Ppoint)<len(Rpoint):
        PRpair = zip(Ppoint,Rpoint[1:])
    elif len(Ppoint)==len(Rpoint):
        if Rpoint[0]<Ppoint[0]:
            PRpair = zip(Ppoint[:-1],Rpoint[1:])
        elif Rpoint[0]>Ppoint[0]:
            PRpair = zip(Ppoint,Rpoint)
    PRpair_ar = np.array(list(PRpair)).astype(int)
    Pamount = ts[PRpair_ar[:,0].tolist()]-ts[PRpair_ar[:,1].tolist()]
    Pseries = np.zeros(ts.ravel().shape)
    for row in np.arange(len(PRpair_ar)):
        ppair = PRpair_ar[row,:]
        Pseries[ppair[0]+1:ppair[1]+1] = -1*Pamount[row]/(ppair[1]-ppair[0])
    
    ## adjust value in case of StartWithDrop and chkStartDrop 
    if (ChkStartDrop and StartWithDrop):
        Pseries[0]=Pseries[1]

    return PRpair_ar,Pamount,Pseries,Ppoint,Rpoint

def makePumpTs(keepECimf,plot = False,show = True,figsize = (8,2),
    savedir = None,title = 'TS',ChkStartDrop= True,ChkEndDrop = True):
    if ((plot)&(~show)):
        makedir(savedir)
    Pseries_list = []
    Pamount_list= []
    PRpair_ar_list = []
    for i in np.arange(len(keepECimf)):
        ts = keepECimf[i].ravel()
        Plt_TS = normalize(ts)        
        PRpair_ar,Pamount,Pseries,Rhigh,Rlow = makeAPumpTs(ts,
            ChkStartDrop= ChkStartDrop,ChkEndDrop = ChkEndDrop)
        Pseries_list.append(Pseries)
        Pamount_list.append(Pamount)
        PRpair_ar_list.append(PRpair_ar)
        if plot:
            fig, ax = plt.subplots(figsize =figsize)
            ax.plot(Plt_TS,zorder = 0,c = 'darkorange')
            ax.scatter(Rhigh,Plt_TS[Rhigh],c = 'red',s = 5)
            ax.scatter(Rlow,Plt_TS[Rlow],c = 'green',s = 5)
            ax.plot(normalize(Pseries))
            ax.set_title(title+'%d'%(i+1))
            ax.grid()
            if show:
                plt.show()
            else:
                plt.savefig(savedir+'/EC%s_Ppatn.png'%str(i+1));
    return Pseries_list,Pamount_list,PRpair_ar_list

def Pseries2GridQ(Pseries_list,EOFs,Sc,gridArea,threshold,showpros = False):
    '''
    Pseries_list: have to be list include array
    Sc: Storage cooeficient.2D array, row is time ,columns s grid number
    gridArea: 2D array, row is time ,columns s grid number
    threshold:

    '''
    Lpumpdepth = np.zeros((len(Pseries_list[0]),len(EOFs)))
    for EOFn in np.arange(len(Pseries_list)):
        gridP_ = np.dot(Pseries_list[EOFn].reshape(-1,1),EOFs[:,[EOFn]].T)
        gridP = np.where(abs(gridP_)<(threshold),0,gridP_)
        Lpumpdepth = Lpumpdepth+gridP
        if showpros:
            print(EOFn)
    LpumpQ = Lpumpdepth*Sc*gridArea
    return LpumpQ,Lpumpdepth

def findPRtime(Pmat,findid):
    '''
    Pmat is dataframe
    findid is grid code
    '''
    findts = Pmat.loc[:,findid]
    pttss = np.zeros(findts.shape)
    rttss = np.zeros(findts.shape)
    for gnum,gid in enumerate(findid):
        TTs = findts.loc[:,gid].values
        if TTs.sum() ==0:
            continue
        fmode = 1
        for n,i in enumerate(TTs):
            if ((i!=0)&(fmode)):
                pttss[n,gnum]=1
                fmode=0
                continue
            if ((i== 0)&(~fmode)):
                rttss[n,gnum]=1
                fmode=1
                continue
    pttsspd = pd.DataFrame(pttss,index = findts.index,columns = findts.columns)
    rttsspd = pd.DataFrame(rttss,index = findts.index,columns = findts.columns)
    return pttsspd,rttsspd

def PRprop(pttsspd,rttsspd,savedir = None,fileprefix = None):
    if pttsspd.sum().sum() ==0:
        print('No pumping')
        return
    pttss = pttsspd.values
    rttss = rttsspd.values
    
    ## duration
    dulist = []
    for i in np.arange(rttss.shape[1]):
        try:
            adu = (np.where(rttss[:,i]==1)[0]-np.where(pttss[:,i]==1)[0])
        except:
            pdb.set_trace()
        for j in adu:
            dulist.append(j)
    dura_pd = pd.DataFrame(pd.Series(dulist).value_counts()).sort_index()
    plt.figure()
    plt.bar(dura_pd.index,dura_pd.values.ravel(),width= 1)
    plt.grid()
    if dura_pd.index.max()>24:
        maxdura = dura_pd.index.max()
    else:
        maxdura = 24
    plt.xticks(np.arange(1,maxdura+1))
    plt.xlim(0,maxdura)
    plt.title('Pumping duration(Hour)')
    plt.xlabel('小時',fontproperties = fontp)
    plt.ylabel('次數',fontproperties = fontp)
    if savedir is not None:
        plt.savefig(savedir+'/'+fileprefix+'Pumping_duration(Hour).png')
    else:
        plt.show()
    plt.clf()

    
    #Start and end point
    p24t = pttss.sum(axis=1).reshape(-1,24).T.sum(axis = 1)
    r24t = rttss.sum(axis=1).reshape(-1,24).T.sum(axis = 1)
    plt.figure()
    plt.plot(p24t,label = 'Start')
    plt.plot(r24t,label = 'End')
    plt.grid()
    plt.xticks(np.arange(24))
    plt.xlabel('點',fontproperties = fontp)
    plt.ylabel('次數',fontproperties = fontp)
    plt.legend()
    if savedir is not None:
        plt.savefig(savedir+'/'+fileprefix+'Pumping_SEtime.png')
    else:
        plt.show()
    plt.clf()
    return dura_pd
def mfvalid2Pcgrid(Z,xpix,ypic,pltCMS,ibound_L):
    '''
    Modflow valid Grid to Pcolor Grid
    '''
    Zi = np.ones(len(pltCMS))*np.nan
    valid_grid = ibound_L.ravel().astype(bool).ravel()
    Zi[valid_grid] = Z
    minx,maxx,miny,maxy = pltCMS[:,0].min(),pltCMS[:,0].max(),pltCMS[:,1].min(),pltCMS[:,1].max()
    xx=np.linspace(minx,maxx,xpix)
    yy=np.linspace(miny,maxy,ypic)
    xi, yi=np.meshgrid(xx,yy) 
    zi = griddata(pltCMS,Zi.ravel(),(xi,yi),method='nearest')
    return xi,yi,zi

def AddZSbmcounty(ax,grididcc,BmPath='../Plot_layer/background.tif',
    SHPpath = '../Plot_layer/ZhuShui_county.shp',pltshpname = True,SHPfontsize = 8):
    maxx,minx,maxy,miny = grididcc.values[:,0].max(),grididcc.values[:,0].min(),grididcc.values[:,1].max(),grididcc.values[:,1].min()
    ZScountySHP = geopandas.read_file(SHPpath)
    ax = ZScountySHP.geometry.boundary.plot(color=None,edgecolor='k',linewidth = 1,ax=ax) 
    pltx,plty = ZScountySHP.centroid.x,ZScountySHP.centroid.y
    if pltshpname:
        for n,i in enumerate(ZScountySHP['TOWNNAME']):
            ax.text(pltx[n],plty[n],i,fontproperties = fontp,va = 'center',fontsize = SHPfontsize,
                     ha = 'center', path_effects=[pe.withStroke(linewidth=4, foreground="white")])
    ax = AddBasemap(ax,BmPath)
    ax.set_ylim(minx-3000,maxx+3000)
    ax.set_ylim(miny-3000,maxy+3000)
    return ax





