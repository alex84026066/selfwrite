import numpy as np
import pandas as pd
import os,emd,pdb
from PyEMD import EMD,EEMD
from selfwrite.EOF_analyse import idw_fill_missingdata
import matplotlib.pyplot as plt
from scipy.stats import norm
from selfwrite.general import makedir,normalize
from selfwrite.general import nearestneighbor
from sklearn import mixture
from sklearn.cluster import KMeans
from scipy import stats
from selfwrite.TempPack.WRPIfunc import makePumpTs
def eofclean(EOFs):
    EOFscl = np.zeros((EOFs.shape))
    for i in np.arange(len(EOFs.T)):
        aeof = EOFs[:,[i]]
        Mv = np.max(aeof)
        if Mv<0.6:
            n_clus = 4
        else:
            n_clus=2
        kmeans = KMeans(n_clusters=n_clus, random_state=0).fit(abs(aeof))
        delnum = stats.mode(kmeans.labels_)[0][0]
        aeof = np.where(kmeans.labels_==delnum,0,aeof.ravel())
        EOFscl[:,i] = aeof
    return EOFscl

# -*- coding: utf-8 -*-
def EOFcleanGM(EOFs):
    '''Data classfication
        EOFs is columns-wise
    '''
    cleanEOFs = EOFs.copy()
    for n,zi in enumerate(EOFs.T):
        clf = mixture.GaussianMixture(n_components=2,covariance_type='full',tol=0.000000001)
        eofzi=zi.reshape((zi.size,1))
        clf.fit(eofzi)
        wei=clf.weights_
        mea=clf.means_
        cov=clf.covariances_
        sel_id=np.where(mea==mea.max())[0]
        EOFzi_classlabel=clf.predict(eofzi).reshape(eofzi.size,1) # preform classification on EOF1
        selectd_min=eofzi[EOFzi_classlabel==sel_id].min()
        unselectd_max=eofzi[EOFzi_classlabel!=sel_id].max()
        cutval=(selectd_min+unselectd_max)/2
        beta=norm.cdf(cutval,loc=mea[sel_id],scale=np.sqrt(cov[sel_id]))
        alpha=1-norm.cdf(cutval,loc=mea[np.abs(1-sel_id)],scale=np.sqrt(cov[np.abs(1-sel_id)]))

#         print (cutval)
#         print (beta)
#         print (alpha)

        cleanEOFs[:,n] = np.where(EOFzi_classlabel.ravel()==sel_id,zi.ravel(),0)

    return cleanEOFs

def EOFExpVarPlt(expvar,need_EOF_num,savedir = None,figsize = (12,3.5)):
    nbar = len(expvar)
    plt.figure(figsize=figsize)
    csExpVar = np.cumsum(expvar*100)
    c = []
    for n in range(nbar):
        if n+1 in need_EOF_num:
            c.append('orangered')
        else:
            c.append('silver')
    plt.bar(np.arange(nbar)+1,csExpVar,color = c)
    plt.grid()
    plt.xlim(0,nbar+1)
    plt.ylim(0,130)
    plt.xticks(range(1,nbar+1))
    plt.yticks(range(0,120,20))
    for n,txt in enumerate(csExpVar):
        plt.text(n+0.52,txt+5,'%0.2f'%txt,fontsize = 10,)
    plt.xlabel('EOF')
    plt.ylabel('Cumulative Explained vairation(%)')
    if savedir is not None:
        plt.savefig('%s/EofExpVar.png'%savedir)
    else:
        plt.show()

## following function is write for calculate Each well Q
def DealNon1IdxDf(df):
    '''
    Deal with grid that has multiple nearest pumping well.
    '''
    ## find index lengh is not 1 and pop them
    lenlist = []
    for i in df.index:
        lenlist.append(len(i))
    popdf = df.loc[np.array(lenlist)!=1,:]
    keepdf = df.loc[np.array(lenlist)==1,:]
    
    ## use for loop create new of that index lengh is not one
    ## distribute each index into different row and values equal
    ## raw_value/index_lengh
    keepdf.index = np.array(keepdf.index.tolist()).ravel()
    for num,i in enumerate(popdf.index):
        arowv = popdf.values.reshape(len(popdf.index),-1)[num,:]
        adddf = pd.DataFrame([])
        for j in i:
            try:
                aidxdf = pd.DataFrame(arowv.ravel()/len(i),
                                      index = [j],columns = df.columns)
            except:
                aidxdf = pd.DataFrame(arowv.ravel()/len(i),
                                      index =df.columns ,columns = [j]).T
            adddf = pd.concat((adddf,aidxdf),axis = 0)
        ## combine additional df into raw df
        keepdf = pd.concat((keepdf,adddf),axis = 0)
    return keepdf

def makeECimfsPumpTs(ts,sltimfs):
    needimfs = []
    if sltimfs is not None:
        ## imf decompose ec
        emd = EMD()
        IMFs = emd(ts.ravel())  

        ## slt specific period
        lper = sltimfs[0]
        uper = sltimfs[1]
        AvgPers = CalimfsAvgPer(IMFs)
        for j in np.arange(len(IMFs)):
            if ((AvgPers[j]>=lper)&(AvgPers[j]<=uper)):
                needimfs.append(IMFs[j])
    else:
        needimfs.append(ts)
        
    ## makePumpTs
    ECPseries,_,_ = makePumpTs(needimfs)
    ECPseries = np.array(ECPseries)
    
    ## Tranfer EC as a 3d array of Pumpdepth of each needed imf
    ECPseries = ECPseries.reshape(len(ECPseries),ECPseries.shape[1],1)
    return ECPseries

def QtsaddinNearP(LpumpP,Invwelrcdf,colname,idx,nrow):
    ## accumulating grid withdrawal into its nearest pumping well
    ## which involve in these pattern
    import warnings
    LpumpQ = LpumpP.sum(axis = 0).reshape(nrow,-1)
    cols = LpumpQ.shape[-1]
    warnings.filterwarnings('ignore',category = np.VisibleDeprecationWarning)
    rnum,cnum = np.where(LpumpQ !=0)
    rcnum = np.vstack((rnum,cnum)).T
    Q = []
    Qts = []
    for r,c in rcnum:
        Q.append([LpumpQ[r,c]])
        Qts.append(LpumpP[:,cols*r+c])
    Qts = np.array(Qts)
    try:
        fpidx, npidx = nearestneighbor(pd.DataFrame(rcnum),Invwelrcdf,False)
    
        aEOFwelQ = pd.DataFrame(Q,index = np.array(npidx).ravel(),
                                columns = [colname])
        aEOFwelQts = pd.DataFrame(Qts,index = np.array(npidx).ravel())
    except:
        pdb.set_trace()
    try:
        aEOFwelQ = aEOFwelQ.reset_index().groupby(by ='index').sum()
        aEOFwelQts = aEOFwelQts.reset_index().groupby(by ='index').sum()
    except:  
        print(colname+' index has multiple values')
        aEOFwelQ = DealNon1IdxDf(aEOFwelQ)
        aEOFwelQts = DealNon1IdxDf(aEOFwelQts)
        aEOFwelQ = aEOFwelQ.reset_index().groupby(by ='index').sum()
        aEOFwelQts = aEOFwelQts.reset_index().groupby(by ='index').sum()
    aEOFwelQ = aEOFwelQ.reindex(idx).fillna(0)
    aEOFwelQts = aEOFwelQts.reindex(idx).fillna(0)
    return aEOFwelQ,aEOFwelQts

def Pseries2GridQ(need_ECs,need_EOFs,Sc,gridArea,threshold,nrow,
                  sltimfs = None,showpros = False,norm=None,
                  clipbools =None,CalPumpWellQ=False,wel_loc = None,
                  InvWelBf = 1):
    '''
    Pseries_list: have to be list include array
    Sc: Storage cooeficient.2D array, row is time ,columns s grid number
    gridArea: 2D array, row is time ,columns s grid number
    threshold:

    '''
    def CalEOFInvWelrcdf(EOF,wel_loc,srbuf=1):
        ##srbuf : searching buffer of include well, if all EOF values in well buffer equal 0, then exclude that well 
        ## identify which well involve in this pattern 
        welrcdf = pd.DataFrame(np.array(wel_loc)[:,1:],
                     index = ['Well%d'%(i+1) for i in np.arange(len(wel_loc))],
                    columns = ['Row','Col'])
        welrcdf['Col']-=1 ## change to 40*38 matrix scale
        dpidx = []
        for weln,i in enumerate(welrcdf.values):
            EOF0n,count = 0,0
            for row in np.arange(-srbuf,srbuf+1):
                for col in np.arange(-srbuf,srbuf+1):
                    EOFv = EOF.reshape(nrow,-1)[i[0]+row,i[1]+col]
                    count+=1
                    if EOFv==0:
                        EOF0n+=1
            if (EOF0n == count):
                dpidx.append(weln)
        Invwelrcdf = welrcdf.drop(index=welrcdf.index[dpidx])
        return Invwelrcdf
    
    EstwelQ = pd.DataFrame([])    
    EstwelQts = pd.DataFrame([])   
    tlen = len(need_ECs.T)
    clipbools=[True]*len(need_ECs) if clipbools is None else clipbools
    clipbools = np.array(clipbools) if not isinstance(clipbools,np.ndarray) else clipbools
    Lpumpdepth = np.zeros((clipbools.sum(),len(need_EOFs)))
    EOFs = np.where(need_EOFs<0,0,need_EOFs)
    EOFs_nag = abs(np.where(need_EOFs>0,0,need_EOFs))
    
    for EOFn in np.arange(tlen):
        Pseries = makeECimfsPumpTs(need_ECs[:,EOFn],sltimfs)
        Pseries_nag = makeECimfsPumpTs(-need_ECs[:,EOFn],sltimfs)
        gridP_ = np.dot(Pseries[:,clipbools,:],EOFs[:,[EOFn]].T).sum(axis = 0)
        gridP_nag = np.dot(Pseries_nag[:,clipbools,:],EOFs_nag[:,[EOFn]].T).sum(axis = 0)
        if norm is not None:
            gridP_ = np.dot(gridP_, np.diag(norm))
            gridP_ = np.dot(gridP_, np.diag(norm))
        gridP = np.where(abs(gridP_)<(threshold),0,gridP_)
        gridPnag = np.where(abs(gridP_nag)<(threshold),0,gridP_nag)
        
        Lpumpdepth = Lpumpdepth+gridP+gridPnag
        
        if CalPumpWellQ:
            idx = ['Well%d'%(i+1) for i in np.arange(len(wel_loc))]
            Invwelrcdf = CalEOFInvWelrcdf(need_EOFs[:,EOFn],wel_loc,srbuf=InvWelBf)
            if Invwelrcdf.empty:
                print('EOF%s do not have value on or around every wells, this component well be ignore'%EOFn)
                continue
            aEOFwelQpos,aEOFwelQtspos = QtsaddinNearP(gridP*gridArea*Sc,Invwelrcdf,'EOF%s'%(EOFn),idx,nrow)
            aEOFwelQnag,aEOFwelQtsnag = QtsaddinNearP(gridPnag*gridArea*Sc,Invwelrcdf,'EOF%snag'%(EOFn),idx,nrow)
            EstwelQ = pd.concat((EstwelQ,aEOFwelQpos,aEOFwelQnag),axis = 1)
            EstwelQts = pd.concat((EstwelQts,aEOFwelQtspos,aEOFwelQtsnag),axis = 0)
        
        if showpros:
            print(EOFn)
    LpumpQ = Lpumpdepth*gridArea*Sc
    if CalPumpWellQ:
        EstwelQts = EstwelQts.reset_index().groupby(by ='index').sum().T
        return LpumpQ,Lpumpdepth,EstwelQ,EstwelQts
    else:
        return LpumpQ,Lpumpdepth


def EstQgirdplt(EstPump,gridxy,RawPump,savedir = None,
                cmap = 'Blues_r',logscale=False,fsufix ='',
               vmax = None,vmin = None):
    LpumpQmean = EstPump.sum(axis = 0)
    QonGrid = EstPump.sum(axis=0)
    if logscale:
        QonGrid = np.log(-QonGrid+1) 
    plt.figure()
    plt.scatter(gridxy[:,0],gridxy[:,1],c = QonGrid,marker = 's',
                cmap = cmap,vmax = vmax,vmin = vmin)
    plt.title('God       :%d \n Estimate: %d'%(RawPump.sum().sum(),EstPump.sum()))
    plt.colorbar()
    if savedir is not None:
        makedir(savedir)
        savep= os.path.join(savedir,'EstQgridPlt%s.png'%(fsufix))
        if logscale:
            savep= os.path.join(savedir,'EstQgridPlt_log%s.png'%(fsufix))
        
        plt.savefig(savep)
    else:
        plt.show()



def WellEstPump(EstPump,RawPump,xidx = None,savedir = None,fsufix = ''):
    if savedir is not None:
        makedir(savedir)
    wellocpumpest = []
    if xidx is None:
        xidx = range(len(EstPump))
    for num in np.arange(len(EstPump.T)):
        plt.figure(figsize = (10,2.5))
        plt.plot(xidx,EstPump.values[:,num])
        plt.plot(xidx,RawPump.values[:,num],label = 'Well%s'%(num+1))
        plt.xlabel('hour',fontsize = 14)
        plt.ylabel('$m^3$/hr',fontsize = 14)
        plt.legend(loc = 'lower left',fontsize = 14)
        aEstQ = EstPump.values[:,num].sum()
        aRawQ = RawPump.values[:,num].sum()
        title = 'Raw:Est = %s:%s'%(int(aRawQ),int(aEstQ))
        plt.title(title)
        plt.grid()
        plt.tight_layout(w_pad=1)
        if savedir is not None:
            savep= os.path.join(savedir,'EstPonWell%s%s.png'%(num+1,fsufix))
            plt.savefig(savep)
        else:
            plt.show()