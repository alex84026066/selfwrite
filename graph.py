import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import matplotlib.dates as mdates
import shapefile as shp
import geopandas as gpd
import gdal
from shapely.geometry import Point
from selfwrite.general import checkdf2array
from scipy.interpolate import griddata
fontpath='C:\\Windows\\Fonts\\msjh.ttc'
fontp = fm.FontProperties(family=u'Source Han Sans TW',fname=fontpath)

import matplotlib.colors as colors
class MidpointNormalize(colors.Normalize):
    """
    Normalise the colorbar so that diverging bars work there way either side from a prescribed midpoint value)

    e.g. im=ax1.imshow(array, norm=MidpointNormalize(midpoint=0.,vmin=-100, vmax=100))
    """
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y), np.isnan(value))

def SetChTitle(ax,title,**kwargs):
    ax.set_title(title,fontproperties = fontp,**kwargs)
    return ax

def makePcgrid(ptxy,ptz,xpix = 100,ypix = 100,extend = None,method = 'neartest',shpcut = None):
    '''Make pcolor input parameter from point data.
    
    Syntax
    ------
        xi,yi,zi = makePcgrid(ptxy,ptz,xpix = 100,ypic = 100,method = 'neartest',shpcut = None)

    Parameters
    ----------
    ptxy: [n x 2].Numpy array of DataFrame.
        Coordinate of points.

    ptz: [n x 1].Numpy array of DataFrame.
        Value of points.

    xpix,ypic: int.
        grid resolution your want to make.

    method: {‘linear’, ‘nearest’, ‘cubic’}, optional
        interpolate method of scipy.griddata.
    
    shpcut: str.optional.
        If ploted grid cut by shapefile. 
        You should input the path of shapefile if shpcut is used.

    Returns
    -------
    xi,yi,zi: [ypic x xpix].
        Input of matplotlib pcolormesh.
    '''
    [ptxy,ptz] = checkdf2array(ptxy,ptz)
    minx,maxx,miny,maxy = ptxy[:,0].min(),ptxy[:,0].max(),ptxy[:,1].min(),ptxy[:,1].max()
    if extend is not None:
        minx,maxx,miny,maxy = minx+extend[0],maxx+extend[1],miny+extend[2],maxy+extend[3]
    xx=np.linspace(minx,maxx,xpix)
    yy=np.linspace(miny,maxy,ypix)
    xi, yi=np.meshgrid(xx,yy) 
    zi = griddata(ptxy,ptz.ravel(),(xi,yi),method=method)
    if shpcut is not None:
        SHP = gpd.read_file(shpcut)
        gmtries = SHP[['geometry']]
        gmtries = gmtries.assign(dscode = '1')
        shpbound = gmtries.dissolve(by='dscode')
        for coln in np.arange(zi.shape[1]):
            for rown in np.arange(zi.shape[0]):
                p1 = Point(xi[rown,coln],yi[rown,coln])
                if not p1.within(shpbound.iloc[0,0]):
                    zi[rown,coln] = np.nan
    return xi,yi,zi

def AddBasemap(ax,basemap,Dimen3D = False,basemap_z = None):
    '''Add basemap in 3D or 2D plot.
    
    Syntax
    ------
        ax = AddBasemap(ax = ax ,basemap = '../basemap.tif',Dimen3D = True,
        basemap_z = -400)

    Parameters
    ----------
    ax: axes.Axes object.
        The axes you want to add basemap.
        
    basemap: str.
        Path of basemap file.
        basemap should be .tif format.
        
    Dimen3D: bool.
        If add basemap on 3D plot.
        
    basemap_z: float.
        Only used while Dimen3D = True.
        The z-coordinate that your basemap want to put. 
        
    extend: list, [minx,maxx,miny,maxy].
        Only used while Dimen3D = True.
        if None, your extend of basemap will be used.
    
    Returns
    -------
    ax: axes.Axes object.
        The ax that has added basemap.  
    
    '''
    ds = gdal.Open(basemap)
    width,height= ds.RasterXSize,ds.RasterYSize
    gt = ds.GetGeoTransform()
    left = gt[0]
    bottom = gt[3] + width*gt[4] + height*gt[5] 
    right = gt[0] + width*gt[1] + height*gt[2]
    top = gt[3]
    basemap = plt.imread(basemap)
    if Dimen3D:
        if extend is None:
            extend = [left,right,bottom,top]
        [minx,maxx,miny,maxy] = extend
        MpRds_x,MpRds_y = np.linspace(left,right,width),np.linspace(bottom,top,height)
        BmSltx_bool,BmSlty_bool = ((minx<=MpRds_x)*(MpRds_x<=maxx)),((miny<=MpRds_y)*(MpRds_y<=maxy))
        Basemap_x,Basemap_y = MpRds_x[BmSltx_bool],MpRds_y[BmSlty_bool]
        Bm_xx,Bm_yy = np.meshgrid(Basemap_x,Basemap_y)
        Bm_rgb = basemap[BmSlty_bool,:,:][:,BmSltx_bool,:]      
        ax.plot_surface(Bm_xx,Bm_yy,np.ones((Bm_yy.shape))*basemap_z,rstride=5,cstride=5,facecolors=Bm_rgb[::-1,:,:]/255.,shade=False,zorder=0)
    else:
        ax.imshow(basemap,extent=[left,right,bottom,top])
    return ax

def addSHP(shpPath,ax,lw = 0.3,c= 'black',**kwargs):
    '''Add Shapefile on existial axes.

    Syntax
    ------
        ax = addSHP(shpPath,ax,lw = 0.3,c= 'black')

    Parameters
    ----------
    shpPath: str.
        Path of shapefile file.

    ax: axes.Axes object.
        The axes you want to add shapefile.

    lw: float.
        linewidth of shapefile.

    c: str.
        color of shapefile line.

    Returns
    -------
    ax: axes.Axes object.
        The ax that has added shapefile.  
    '''
    SHP = gpd.read_file(shpPath)
    shpax = SHP.geometry.boundary.plot(color=None,edgecolor=c,linewidth =lw,ax=ax,**kwargs)
    return ax

def scatterBMplot(plot_loc,plot_value,basemap,pltname = None,namelocadj = None,figsize = None,title = None,titlefs = None,
    savefigname = None,show = True,CbarTickslog2value = False,**kwargs):
    [plot_loc,plot_value,pltname] = checkdf2array(plot_loc,plot_value,pltname)
    ds = gdal.Open(basemap)
    width = ds.RasterXSize
    height = ds.RasterYSize
    gt = ds.GetGeoTransform()
    left = gt[0]
    bottom = gt[3] + width*gt[4] + height*gt[5] 
    right = gt[0] + width*gt[1] + height*gt[2]
    top = gt[3]
    fig = plt.figure(figsize= figsize)
    ax = fig.add_subplot(111)
    basemap = plt.imread(basemap)
    ax.imshow(basemap,extent=[left,right,bottom,top])
    minx,maxx = plot_loc[:,0].min()-2000,plot_loc[:,0].max()+2000
    miny,maxy = plot_loc[:,1].min()-2000,plot_loc[:,1].max()+2000
    SC = ax.scatter(plot_loc[:,0],plot_loc[:,1],c = plot_value.ravel(),**kwargs)
    if pltname is not None:
        if namelocadj is None:
            namelocadj = dict()
            for i in np.arange(len(pltname)):
                namelocadj[i] = [0,0]
        for i in np.arange(len(pltname)):
            try:
                namelocadj[i]
            except:
                namelocadj[i] = [0,0]
            ax.text(plot_loc[i,0]+namelocadj[i][0],plot_loc[i,1]+namelocadj[i][-1],pltname[i],fontproperties = fontp)
    if not isinstance(plot_value[0],np.str_):
        cb = fig.colorbar(SC)
    if CbarTickslog2value:
        Colorbarlog2value(cb)
    ax.set_xlim(minx-3000,maxx+3000)
    ax.set_ylim(miny-3000,maxy+3000)
    if title is not None:
        ax.set_title(title,fontproperties=fontp,fontsize = titlefs)
    if savefigname is not None:
        plt.savefig(savefigname)
    else:
        if not show:
            return ax
        plt.show()
    return ax

def Colorbarlog2value(cb):
    '''This function could change logscale value showed on colorbar to normal scale value. 

    Syntax
    ------
        Colorbarlog2value(cb)

    Parameters
    ----------
    cb: colorbar object.
        
    Returns
    -------
    
    '''
    
    cbticks = np.round(cb.get_ticks(),2)
    showcbticks = ['$10^{%s}$'%str(i) for i in cbticks]
    for i in np.arange(len(cbticks)):
        if (int(cbticks[i]) == cbticks[i]):
            showcbticks[i] = '$10^{%s}$'%str(int(cbticks[i]))
    cb.ax.set_yticklabels(showcbticks)
    return